---
title: HTML
menu: foundation
weight: 4
---
HTML5 is a bit more sloppy than its predecessors, that's why we set guidelines to keep it tidy, clean and well-formed.
A consistent use of style, makes it easier for others to understand your HTML.

## General Formatting

- Use soft tabs with two spaces, they're the only way to guarantee code renders the same in any environment.
- Nested elements should be indented once (two spaces).
- Always use double quotes, never single quotes, on attributes.
- Always use lower case element names & lower case attribute names (DOCTYPE is the only exception).
- Avoid spaces around equal signs, avoid long code lines and do not add blank lines without a reason.
- In HTML5, it is optional to close empty elements. Although if you expect XML software to access your page, it is a good idea to keep the closing slash.
- Don't omit optional closing tags (e.g. `</li>` or `</body>`).

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Page title</title>
  </head>
  <body>
    <img src="images/sweetmustard-logo.png" alt="Sweet Mustard"/>
    <h1 class="hello-world">Hello, world!</h1>
  </body>
</html>
```

## HTML5 doctype

Enforce standards mode and more consistent rendering in every browser possible by adding this simple doctype at the beginning of every HTML page.

```html
<!DOCTYPE html>
<html>
  <head>
  </head>
</html>
```


## Language attribute

From the HTML5 spec:

>Authors are encouraged to specify a lang attribute on the root html element, giving the document's language. This aids speech synthesis tools to determine what pronunciations to use, translation tools to determine what rules to use, and so forth.
Read more about the `lang` attribute in the [spec](http://w3c.github.io/html/semantics.html#the-html-element).

Head to Sitepoint for a [list of language codes](https://www.sitepoint.com/iso-2-letter-language-codes/).

```html
<html lang="en-us">
  <!-- ... -->
</html>
```

## Viewport

The viewport is the user's visible area of a web page. This varies with the device, and will be smaller on a mobile phone than on a computer screen. HTML5 introduced a method to let web designers take control over the viewport, through the `<meta>` tag. The following tag should always be included:

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```

The `width=device-width` part sets the width of the page to follow the screen-width of the device (which will vary depending on the device). The `initial-scale=1.0` part sets the initial zoom level when the page is first loaded by the browser.

## IE compatibility mode

Internet Explorer supports the use of a document compatibility `<meta>` tag to specify what version of IE the page should be rendered as. Unless circumstances require otherwise, it's most useful to instruct IE to use the latest supported mode with __edge mode__.

For more information, [read this awesome Stack Overflow article](https://stackoverflow.com/questions/6771258/what-does-meta-http-equiv-x-ua-compatible-content-ie-edge-do).

```html
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
```

## Character encoding

Quickly and easily ensure proper rendering of your content by declaring an explicit character encoding. When doing so, you may avoid using character entities in your HTML, provided their encoding matches that of the document (generally UTF-8).

```html
<head>
  <meta charset="UTF-8">
</head>
```

## CSS and JavaScript includes

Per HTML5 spec, typically there is no need to specify a `type` when including CSS and JavaScript files as `text/css` and `text/javascript` are their respective defaults.

### HTML5 spec links

- [Using link](https://www.w3.org/TR/2011/WD-html5-20110525/semantics.html#the-link-element)
- [Using style](https://www.w3.org/TR/2011/WD-html5-20110525/semantics.html#the-style-element)
- [Using script](https://www.w3.org/TR/2011/WD-html5-20110525/scripting-1.html#the-script-element)

```html
<!-- External CSS -->
<link rel="stylesheet" href="code-guide.css">

<!-- In-document CSS -->
<style>
  /* ... */
</style>

<!-- JavaScript -->
<script src="code-guide.js"></script>
```

### Performance

When there's no reason to load your script before your content, don't block the rendering of your page. Try to split up too heavy style sheets and isolate the styles that are absolutely required initially and defer the loading of the secondary declarations in a separate style sheet.

## Use of div's and HTML5 element

In previous versions of HTML, the `div` element was defined to be a generic element for structuring a page. HTML5 introduced new semantic elements as `header`, `section` and `article`.
We strive to not overuse the `div` element and use HTML5 elements where possible.

The `header` element should be used at the top of your page and can include a group of introductory or navigational aids. An important note is that you are not restricted to using one `header` element per site. Each section can have its own header. Just like the header element, we can use the `footer` element multiple times on a page. It can include information about its section such as the author, links to documents and copyright data.

The `aside` element was introduced for content tangentially related to the contect surrounding it. It's crucial to remain aware of its context, when used within an `article` element, the contents should be related to that article. When used outside of an article element, the content should be related to the site.

The difference between the `article` and `section` elements often lead to confusing. Thankfully, [the specs](https://www.w3.org/TR/html5/sections.html#the-article-element) give some more information:
> The article element represents a component of a page that consists of a self-contained composition in a document, page, application, or site and that is intended to be independently distributable or reusable, e.g. in syndication. This could be a forum post, a magazine or newspaper article, a blog entry, a user-submitted comment, an interactive widget or gadget, or any other independent item of content.
> The section element represents a generic document or application section…The section element is not a generic container element. When an element is needed only for styling purposes or as a convenience for scripting, authors are encouraged to use the div element instead. A general rule is that the section element is appropriate only if the element’s contents would be listed explicitly in the document’s outline.
So The <article> element is a specialised kind of <section>, it has a more specific semantic meaning than <section> in that it is an independent, self-contained block of related content. We could use <section>, but using <article> gives more semantic meaning to the content. By contrast <section> is only a block of related content, and <div> is only a block of content.
An article element can be used inside a section element like a comment sectoin or the homepage or category pages of a webblog. On the other hand section elements can be used inside a article element to split the article into logical groups of content with headings.

The `nav` element is a section of a page that links to other pages or to parts within the page. A section that consist of major navigation blocks.

`figure` and `figcation` elements are used to mark up diagrams, illustartion, photos and code examples.

```html
<figure>
  <img src="/kookaburra.jpg" alt="Kooaburra">
  <img src="/pelican.jpg" alt="Pelican stood on the beach">
  <img src="/lorikeet.jpg" alt="Cheeky looking Rainbow Lorikeet">
  <figcaption>Australian Birds. From left to right, Kookburra, Pelican and Rainbow Lorikeet.</figcaption>
</figure>
```

Always make sure your links and buttons are marked as such, this improves the accessibility of your website.

## H1

The use of `h2` represents a 2nd level heading, this can be used multiple times. Using multiple `h1`'s is valid in HTML5 but can cause issues with accessibility, the HTML spec advises:
> …the [outline](https://www.w3.org/TR/html5/sections.html#outline) algorithm cannot be relied upon to convey document structure to users. Authors are advised to use heading [rank](https://www.w3.org/TR/html5/sections.html#rank) (h1–h6) to convey document structure.

## Link vs Button

As everybody knows links are a fundamental part of the internet experience, linking pages together. But these days with CSS en Javascript, it is possible to use a button to navigate or make a link look like a button with simple CSS. This can create confusion and wrongful use for these elements.

A link:

- navigates to a page or fragment with href.
- possible cause a browser refresh.
- can deep-link client-rendered apllications.
- open in new windows with target.
- can be activated with TAB and the Enter key.
- shows the URL in the status bar.

But a button:

- submits and resets forms.
- triggers UI changes with JavaScript.
- is operable with Tab and the Space key.
- is focusable.
- has built-in semantics
- has keyboard support for click() event listeners.
- can be disabled.

So a link should always navigate and have a `href`, while a button should be used for basically anything else (Form, Dropdown menu, Modal dialog triggers, Close Buttons, Media Control).
A button has a lot of extra functionality but should not be used for linking, it's also very useful to keep groups of these elements because people with screen readers use different keys to activate both.

## Practicality over purity

Strive to maintain HTML standards and semantics, but not at the expense of practicality. Use the least amount of markup with the fewest intricacies whenever possible.

## Image Attributes

Always add the `alt` attribute to images, this is important when the image cannot be displayed. Always use a useful alt-text to improve the accessibility.
Defining the height and width can be useful since it reduces flickering because the browser reserves space for the image before loading.
```
<img src="html5.gif" alt="HTML5">
```

## Attribute order

HTML attributes should come in this particular order for easier reading of code.

- `src, for, type, href, value`
- `class`
- `id, name`
- `data-*`
- `title, alt`
- `role, aria-*`

Classes make for great reusable components, so they come first. Ids are more specific and should be used sparingly (e.g., for in-page bookmarks), so they come second.

```html
<a class="..." id="..." data-toggle="modal" href="#">
  Example link
</a>

<input class="form-control" type="text">

<img src="..." alt="...">
```

## Boolean attributes

A boolean attribute is one that needs no declared value. XHTML required you to declare a value, but HTML5 has no such requirement.

For further reading, consult the [WhatWG section on boolean attributes](https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#boolean-attributes):

>The presence of a boolean attribute on an element represents the true value, and the absence of the attribute represents the false value.

If you must include the attribute's value, and __you don't need to__, follow this WhatWG guideline:

>If the attribute is present, its value must either be the empty string or [...] the attribute's canonical name, with no leading or trailing whitespace.

__In short, don't add a value.__

```html
<input type="text" disabled>

<input type="checkbox" value="1" checked>

<select>
  <option value="1" selected>1</option>
</select>
```

## Reducing markup

Whenever possible, avoid superfluous parent elements when writing HTML. Many times this requires iteration and refactoring, but produces less HTML. Take the following example:

```html
<!-- Not so great -->
<span class="avatar">
  <img src="...">
</span>

<!-- Better -->
<img class="avatar" src="...">
```
