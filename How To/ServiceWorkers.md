---
title: "Progressive WebApp Development with Service Workers"
menu:
  foundation:
    title: "Progressive WebApp Development: Service Workers"
    parent: "How To"
weight: 2
---


## Introduction

We have all experienced it before, you're browsing a website on your mobile device 
when suddenly your internet connection drops. Having a weak wifi signal or you reaching your mobile data limit could be some of the reasons. Wouldn't it be convenient if you could keep browsing the website offline? New technologies such as service workers make this possible. In this post, I will address this fairly new concept and show how you can add a service worker to your web-application.

This new modality is an idea called 'Offline-First’ web development. Yet, most of us already used websites or web-applications that implement this functionality. Think of Facebook, for example, where you can like posts and create posts, even when there is no internet connection. The timeline you see is 'cached' data, and your actions are also cached. All requests will be sent to the server automatically when you reconnect to the internet and you don't even notice it. Amazing!

## Service Worker

{{< figure src="./../assets/ServiceWorker.png" alt="ServiceWorker" >}}

A service worker is a script that lives in the root of the application, a virtual proxy between the browser and the network. It can detect if there’s an internet connection or not, and it can act in different ways based on the response. 
You most likely already have a few service workers saved in your browser(s). For example, when you use Google Chrome and navigate to `chrome://serviceworker-internals`, a list will be provided. In FireFox active service workers can be viewed by navigating to `about:debugging`.

 {{< figure src="./../assets/ServiceWorkersList.png" alt="ServiceWorkers" >}}



## Hands-on

Let’s get into the basics of how to add a service worker to a web application.

Before we can use service workers, we need to implement `HTTPS` : a secure connection.
First of all, create a blank service worker file called `sw.js` (or `service-worker.js`) in the root of your application. Second, in the web application’s bootstrap script, you can add some code to test for feature support and register the service worker, if it is supported.

```javascript
//scripts.js site.js,...
if ('serviceWorker' in navigator) {
  // Attempt to register it
  navigator.serviceWorker.register('/sw.js').then(function() {
    console.log('ServiceWorker successfully registered');
  }).catch(function(error) {
    console.log('ServiceWorker registration failed: ', error);
  });
}
```

Then we need to give `the service worker` a name (versioning), install it, add the assets we want to cache and activate the service worker. This can be done via adding an event listener, passing the install and activate event. Then we use the fetch event to grab data and use it during connection downtime. The fetch event is simply a pass-through, it will always pass the request to the network. With the `fetchEvent.respondWith()` method, we can prevent the browsers default handling and provide (a promise for) a response manually. In the sample `service worker` below, we’re only logging requests in the fetch event, but the actual requests are still sent to the network.

```javascript
var cacheName = 'ServiceWorkerDemo_V1';

// assets to cache
var assetsToCache = [
    '/',
    '/css/site.css',
    '/Home/Index',
    '/Home/Contact',
    'Home/About'
];

self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(cacheName).then(function (cache) {
            return cache.addAll(assetsToCache);
        }).then(function () {
            return self.skipWaiting();
        })
    );
});

self.addEventListener('activate', function (event) {
    return self.clients.claim();
});

//Here we intercept requests and log them to the console
self.addEventListener('fetch', function (event) {
   console.log(event.request); 
});
```

We can run the application and check the `console` tab in the browser developer tools, it should show if registration has failed or succeeded.

{{< figure src="./../assets/RegisteredLog.png" alt="Registration" >}}

 Under the `application` tab we can see if the `service worker` is active. We can also see all other saved service workers from other domains.

During development it's also recommended to check the `Update on reload` checkbox, this will cause the service worker to update on refresh, so that you don’t have to close the browser again and again. 

{{< figure src="./../assets/ApplicationTab.png" alt="ApplicationTab" >}}

This is the output in the console, after loading the index page. As provided in the fetch event, we can see all requests logged by the service worker.

{{< figure src="./../assets/RequestLogs.png" alt="LoggedRequests" >}}

If we take a look at the `Cache Storage` menu under the `Application` tab we can see the `ServiceWorkerDemo` cache storage is created. The assets we provided in the service workers `cache.adAll()` method, are stored here. 

{{< figure src="./../assets/CashedContent.png" alt="CashedContent" >}}

If we click the `Offline` checkbox in the `Application` tab and disconnect the application from the internet, we can still browse the cached content.

But there’s a remaining problem: when a cache is missing, a request is made but the response is not cached. So if we make the request again, it still propagates over the network. We need to cache the response for that request in order to avoid this. Whether the app is online or offline, we serve content from the cache first. If a specific file is not in the cache storage, the app adds it there before serving it to the client.

Here is the updated fetch event with caching improvement.
```javascript
self.addEventListener('fetch', function(e) {
  e.respondWith(
    caches.match(e.request).then(function(r) {
          console.log('[Service Worker] Fetching resource: '+e.request.url);
      return r || fetch(e.request).then(function(response) {
                return caches.open(cacheName).then(function(cache) {
          console.log('[Service Worker] Caching new resource: '+e.request.url);
          cache.put(e.request, response.clone());
          return response;
        });
      });
    })
  );
});
```

In the above code-snippet, `cache.Put()` is used to cache the entry manually using request and response objects.

So, what if we modify all the static files which could be cached? The cached files should be removed from the cache storage, and replaced with the updated files. This is where the `activate` event is useful. We can update the service worker so the install event is triggered. Inside the install event, we give our `cacheName` an update to a newer version.
After activating the new service worker, we can delete the old cache object in that event handler.

```javascript
var cacheName = 'ServiceWorkerDemo_V2';
...
//This will successfully delete old cached files
self.addEventListener('activate', function(event) {
    event.waitUntil(
        caches.keys()
        .then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cName) {
                    if (cName !== cacheName) {
                        return caches.delete(cName);
                    }
                })
            );
        })
    );
});
```

Using the `navigator.onLine` property, we can manually check if the browser is connected to the internet. Below is a example of the fetch event modified to log the requests, but only when the browser is offline.

```javascript
self.addEventListener('fetch', function (event)
{
	if (!navigator.onLine)
	{
		console.log(event.request);
	}
	event.respondWith(fetch(event.request));
}
```

## Cashing Patterns

In addition, there are several Caching Patterns:

`Network Only` is the first caching pattern. It is what web apps have always done up until service workers. They handle the request by fetching the url from the network. But if the network fails, the whole request fails.

Implementation in fetch event:

```javascript
event.respondWith(
	fetch(event.request)
);
```

The opposite of `Network Only` pattern is the `Cache Only` pattern. In this pattern we try to resolve any url from the cash and only from the cash. When the item is not in the cash, request fails.

Implementation in fetch event:

```javascript
event.respondWith(
    caches.match(event.request)
);
```

In `Cache First` pattern, if the request matches a cache entry, that entry is send to the browser. But when it doesn’t match, then it goes to the network, write the return response to the cache, and then serve it to the browser.

Implementation in fetch event:

```javascript
event.respondWith(
	caches.match(event.request).then(function(cResponse){
	if(cResponse) { return cResponse }
	return fetch(event.request).then(function(fResponse){
	    return caches.open('DemoCache').then(function(cache){
        return cache.put(event.request, 
        fResponse.clone()).then(function(){
		return fResponse;
		});
	     });
	});
	});
);
```


`Network First` is the pattern where handle the request by going to the network first, and only  serve from the cache in failure situations. On every successful request, the response is also saved in the cache, for when the connection is lost in future requests. 

Implementation in fetch event:

```javascript
event.respondWith(
	fetch(event.request)then(function(fResponse) {
		return caches.open(cacheName).then(function(cache) {
		if(!fResponse.ok) {
			return cache.match(event.request);
		} else {
		   cache.put(event.request, fResponse.clone());
		  return fResponse; }
		});
	      }):
	);
```

The final pattern addressed in this article is simply called the `Fastest` pattern. A request is send to the cache and the network at the same time. The fastest response is send back to the browser. Most of the time the response will come from the cache, but every response from the network will also be stored (or updated) in the cache. 

Implementation in fetch event:
```javascript
event.respondWith(() => {
	var promises = [caches.match(event.request),
		fetch(event.request)];
return new Promise((resolve, reject) => {
	promises.map(p => Promise.resolve(promise));
	promises.forEach(p => p.then(resolve));
	promises.reduce((a,b) => a.catch(() => b))
	.catch(() => reject(new Error('Both promises failed.')));
 	});
	}
    );
```

## Offline storage

Further expanding on the subject, there is also the capability to store data offline in the browser. 

`IndexedDB` is a transactional database (NoSQL) in the browser where you can store `JSON` object collections. An indexing system optimizes access to stored objects. You can create an `IndexedDB` inside the service worker activation event and handle offline storage. All browsers currently support `IndexedDB` except for Opera Mini. 

## SW-Toolbox

`SW Toolbox` is a library that simplifies some of the more challenging implementation details of the service worker. It cottifies the caching patterns mentioned in this post, and also handles network timeouts. Install SW Toolbox through NPM, Bower or direct from GitHub:

```
npm install --save sw-toolbox
bower install --save sw-toolbox
git clone https://github.com/GoogleChrome/sw-toolbox.git
```


**Useful Links**:



https://github.com/GoogleChromeLabs/sw-precache

https://googlechromelabs.github.io/sw-toolbox/








