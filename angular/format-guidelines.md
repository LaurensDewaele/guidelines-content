---
title: Format Guidelines
menu:
  foundation:
    title: "Format Guidelines"
    parent: "Angular"
weight: 2
---

In order for everybody to achieve this predifined coding format adding these to your preferred editor is needed.

## .editorconfig
````editorconfig
# Editor configuration, see http://editorconfig.org
root = true

[*]
charset = utf-8
indent_style = space
indent_size = 2
insert_final_newline = true
trim_trailing_whitespace = true

[*.md]
max_line_length = off
trim_trailing_whitespace = false

````


## Prettier

It's important to keep our style consistent before pu`shing something. This is where prettier comes in play. 
Prettier is a tool which formats our code in a predefined format on saving. Multiple ways of installing are possible. Check out the [documentation](https://prettier.io/docs/en/) for more information.

### Installation
Install with npm:
```
npm install --save-dev --save-exact prettier
# or globally
npm install --global prettier
```

### Setup
Next up we'll add to files to our project to the same directory as the `package.json`. These files are:
- `prettier.config.js`
- `.prettieringore`

#### prettier.config.js
````javascript
module.exports = {
  printWidth: 140, 
  tabWidth: 2,    
  useTabs: false,  
  semi: true,
  singleQuote: true,
  trailingComma: 'none', 
  noBracketSpacing: true,
  arrowParens: 'avoid',  
  parser: 'typescript'
};
````

#### .prettierignore
Depending on the settings from this previous step, you might run into some errors where any changes to your package.json will result in a massive reformat. If you manually edit it, Prettier may change all the tabWidth to 2. But if you use npm to install anything, that install will reformat the entire package.json to use tabWidth of 4. To avoid this tabWidth-thrashing, add package.json to your `.prettierignore`.
````markdown
package.json
````

#### tslint.json
To avoid errors with Prettier it's best to remove the following lines from our `tslint.json` file
````markdown
- comment-format
- curly
- eofline
- import-spacing
- indent
- max-line-length
- no-trailing-whitespace
- one-line
- quotemark
- semicolon
- typedef-whitespace
- angular-whitespace
````

### IDE plugin / setup
Next up it's time to set up our IDE to reformat the code anytime you save a file.

#### Visual Code
Install the following plugin [prettier - code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode).

#### Atom
Install the following plugin [prettier - code formatter](https://github.com/prettier/prettier-atom).

#### Webstorm / Intellij
Jetbrains products work differently. Webstorm allready has the 'file watcher' plugin installed, Intellji however does not. So when using Intellji you first need to install this plugin.
Go to **File | Settings | Tools | File Watchers** for Windows and Linux or **WebStorm | Preferences | Tools | File Watchers** for OS X and click + to add a new tool. Let’s name it Prettier.

##### File Watcher
- **File Type:** TypeScript
- **Scope:** Current File
- **Program** set ``prettier`` 
- **Arguments** set ```--write --print-width 140 --single-quote $FilePath$```
- **Working directory** set ```$ProjectFileDir$```
- **Auto-save edited files to trigger the watcher:** Uncheck to reformat on Save only (otherwise code will jump around while you type).

![File Watcher](/images/image(1).png)