---
title: Angular
menu: foundation
weight: 2
---

Angular applications can get quite big, so in order to keep everything straightforward and easy to find we have to follow a set a rules. This consist of folder achitecture as well as some formatting and general code guidelines.

This is an added style guide on the [javascript style guide](../javascript/). So be sure to read that one as well.

## Folder Structure

Folder structure is very important in order to keep a good overview and knowledge of which file is located where, and how many responsibility it has. Keep in mind we want to have as many dumb components as possible.

The main structure in our /src folder is gonna look something like this.

```markdown
├ app/          : The main files for our application
├ assets/       : The location for our fonts, i18n, images, js, ...
├ common/       : Components, directives, guards, utils, ... that can be reused across our application
├ modules/      : Our major modules grouped together
├ styles/       : Our styling
├ index.html    : The main .html file
└ main.ts       : The main .ts file
```

### General Component Structure
In order to keep every component structured, components are sliced into several components. Each presentational module subscribes, through its own sandbox, to events published by the application core. This is an example of a module in our modules folder.

```markdown
├ module1/            : The main files for our application
  ├ components/          : All components
  ├ containers/          : The containers
  ├ services/            : Services for backend calls
  ├ component1.sandbox   : Sandbox component 
  └ index.ts             : The main .ts file for our declarations, imports, etc.
```

### App Folder
The app folder contains all main app component files. These include the routing, navigation, notification, popups, etc. A structure could look something like this.
```markdown
├ app/                              
  ├ components/                    
  | └ navbar/                       
  |   ├ navbar.component.html      
  |   ├ navbar.component.scss       
  |   └ navbar.component.ts         
  ├ containers/                     
  | ├ application/                  
  | └ root/                         
  ├ controllers/                   
  | ├ notifications/                
  | └ popups/                       
  ├ services/                       
  | ├ service1.ts                  
  | └ service2.ts                  
  ├ component1.sandbox              
  └ index.ts                       
```
*Note:* This is just an example, controllers or other components might not be necessary according to your application.

### Assets Folder
The assets folder contains files such as fonts being used, images, translation and the environment(s) as well.
```markdown
├ assets/                              
  ├ fonts/                    
  | ├ fontawesome/  
  | | ├ font.otf     
  | | ├ font.woff      
  | | └ font.ttf                                                            
  ├ i18n/                     
  | ├ en.json                 
  | ├ nl.json                 
  | └ ...                       
  ├ img/                   
  | ├ cars/                
  | └ team.png                       
  ├ js/                       
  | ├ splash.js                  
  | └ ... .js                              
  └ env.js                       
```
Note that there is no index.ts file in the assets folder. Our environment can be set here as well.

### Common Folder
The common folder holds all files that can be used across the entire application. This is also the place where to put pipes, directives and utils.
```markdown
├ common/                              
  ├ components/                                                
  ├ containers/                                           
  ├ directives/                   
  | ├ color/                
  | ├ color-picker/                
  | └ ...               
  ├ guards/                                                
  ├ pipes/                                           
  ├ services/                              
  ├ utils/                              
  ├ types/                :Our models                            
  └ index.ts                       
```
### Modules Folder
Each major module consists out of 4 major block, the components, containers, services and the sanbox.
```markdown
├ module1/                              
  ├ components/   
  | ├ list-item/                
  | | ├ list-item.component.html      
  | | ├ list-item.component.scss       
  | | ├ list-item.component.spec.ts       
  | | └ list-item.component.ts                
  | ├ detail-item/                
  | | ├ detail-item.component.html      
  | | ├ detail-item.component.scss       
  | | ├ detail-item.component.spec.ts       
  | | └ detail-item.component.ts                                             
  ├ containers/               
  | └ component1-container/     
  | | ├ component1.container.html      
  | | ├ component1.container.scss       
  | | ├ component1.container.spec.ts       
  | | └ component1.container.ts                                   
  ├ services/     
  | ├  service1.service.ts   
  | └ service1.service.spec.ts   
  ├ component1.sanbox.ts                                                                         
  ├ component1.sanbox.spec.ts                                                                         
  └ index.ts                       
```

### Styles Folder
This is our base folder structure for our styling. For more information check our [css style guide](../css/).

## Format Guidelines

In order for everybody to achieve this predifined coding format adding these to your preferred editor is needed.

### .editorconfig
````editorconfig
# Editor configuration, see http://editorconfig.org
root = true

[*]
charset = utf-8
indent_style = space
indent_size = 2
insert_final_newline = true
trim_trailing_whitespace = true

[*.md]
max_line_length = off
trim_trailing_whitespace = false

````


### Prettier

It's important to keep our style consistent before pu`shing something. This is where prettier comes in play. 
Prettier is a tool which formats our code in a predefined format on saving. Multiple ways of installing are possible. Check out the [documentation](https://prettier.io/docs/en/) for more information.

#### Installation
Install with npm:
```
npm install --save-dev --save-exact prettier
# or globally
npm install --global prettier
```

#### Setup
Next up we'll add to files to our project to the same directory as the `package.json`. These files are:
- `prettier.config.js`
- `.prettieringore`

##### prettier.config.js
````javascript
module.exports = {
  printWidth: 140, 
  tabWidth: 2,    
  useTabs: false,  
  semi: true,
  singleQuote: true,
  trailingComma: 'none', 
  noBracketSpacing: true,
  arrowParens: 'avoid',  
  parser: 'typescript'
};
````

##### .prettierignore
Depending on the settings from this previous step, you might run into some errors where any changes to your package.json will result in a massive reformat. If you manually edit it, Prettier may change all the tabWidth to 2. But if you use npm to install anything, that install will reformat the entire package.json to use tabWidth of 4. To avoid this tabWidth-thrashing, add package.json to your `.prettierignore`.
````markdown
package.json
````

##### tslint.json
To avoid errors with Prettier it's best to remove the following lines from our `tslint.json` file
````markdown
- comment-format
- curly
- eofline
- import-spacing
- indent
- max-line-length
- no-trailing-whitespace
- one-line
- quotemark
- semicolon
- typedef-whitespace
- angular-whitespace
````

#### IDE plugin / setup
Next up it's time to set up our IDE to reformat the code anytime you save a file.

##### Visual Code
Install the following plugin [prettier - code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode).

##### Atom
Install the following plugin [prettier - code formatter](https://github.com/prettier/prettier-atom).

##### Webstorm / Intellij
Jetbrains products work differently. Webstorm allready has the 'file watcher' plugin installed, Intellji however does not. So when using Intellji you first need to install this plugin.
Go to **File | Settings | Tools | File Watchers** for Windows and Linux or **WebStorm | Preferences | Tools | File Watchers** for OS X and click + to add a new tool. Let’s name it Prettier.

###### File Watcher
- **File Type:** TypeScript
- **Scope:** Current File
- **Program** set ``prettier`` 
- **Arguments** set ```--write --print-width 140 --single-quote $FilePath$```
- **Working directory** set ```$ProjectFileDir$```
- **Auto-save edited files to trigger the watcher:** Uncheck to reformat on Save only (otherwise code will jump around while you type).

![File Watcher](/images/image(1).png)

## Forms

If a Vue component has so many props (or listeners, directives, ...) that they don't fit on one line anymore you need to put every prop on it's own line. Every line needs to be intended with 2 spaces. The closing > goes on a new unintended line followed by the closing tag.


````html
<!-- Good -->
<template>
  <my-component myProp="value"><my-component>
</template>
````
`````html
<!-- Good -->
<template>
  <my-component
    v-if="shouldDisplay"
    myProp="value"
    @change="handleEvent"
  ></my-component>
</template>

<!-- Bad: wrong indentation, closing `>` is not correct placed -->
<template>
  <my-component
    v-if="shouldDisplay"
    myProp="value"
    @change="handleEvent">
  </my-component>
</template>
`````

### Reactive forms vs Template Driven

Template Driven Forms are maybe for simple forms slightly less verbose, but the difference is not significant. Reactive Forms are actually much more powerful and have a nearly equivalent readability.

Most likely in a large-scale application, we will end up needing the functionality of reactive driven forms for implementing more advanced use cases like for example auto-save.

For this reason we'll opt to use reactive forms over template driven ones.

### Template driven forms

Angular provides form-specific directives that you can use to bind the form input data and the model. The form-specific directives add extra functionality and behavior to a plain HTML form. The end result is that the template takes care of binding values with the model and form validation. 

A short example looks something like this

````html
<!-- app.component.html -->

<div class="container">
  <h1>Add user</h1>
  <form #f="ngForm" novalidate (ngSubmit)="save(f.value, f.valid)">
    <div class="form-group">
      <label>Name</label>
      <input type="text" class="form-control" 
        name="name" [(ngModel)]="user.name" 
        #name="ngModel" required minlength="5">
      <small [hidden]="name.valid || (name.pristine && !f.submitted)" class="text-danger">
            Name is required (minimum 5 characters).
          </small>
      <!--<pre>{{name.errors | json}}</pre>-->
    </div>
    <div ngModelGroup="address">
      <div class="form-group">
        <label>Street</label>
        <input type="text" class="form-control" 
          name="street" [(ngModel)]="user.address.street"
          #street="ngModel" required>
        <small [hidden]="street.valid || (street.pristine && !f.submitted)" class="text-danger">
          Street is required.
        </small>
      </div>
      <div class="form-group">
        <label>Post code</label>
        <input type="text" class="form-control" 
          name="postcode" [(ngModel)]="user.address.postcode">
      </div>
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
  </form>
  <div class="margin-20">
    <div>Form details:-</div>
    <pre>Is form valid?: <br>{{f.valid | json}}</pre>
    <pre>Is form submitted?: <br>{{f.submitted | json}}</pre>
    <pre>form value: <br>{{f.value | json}}</pre>
  </div>
</div>
````

And our typescript:
````typescript
// app.component.ts
import { Component, OnInit } from '@angular/core';
import { User } from './user.interface';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
  public user: User;
  
  ngOnInit() {
    this.user = {
      name: '',
        address: {
         street: '',
         postcode: '8000'
      }
    };
  }
  
  save(model: User, isValid: boolean) {
    console.log(model, isValid);
  }
}
````

### Reactive forms
Reactive forms use a `formgroup` directory applied to the whole form. Validators are also placed in the controller instead of in the html itself.

For example:
````html
<section class="sample-app-content">
    <h1>Model-based Form Example:</h1>
    <form [formGroup]="form" (ngSubmit)="onSubmit()">
        <p>
            <label>First Name:</label>
            <input type="text" formControlName="firstName">
        </p>
        <p>
            <label>Password:</label>
            <input type="password" formControlName="password">
        </p>
        <p>
            <button type="submit" [disabled]="!form.valid">Submit</button>
        </p>
    </form>
</section>
````
and our component:
````typescript
import { FormGroup, FormControl, Validators, FormBuilder } 
    from '@angular/forms';

@Component({
    selector: "model-driven-form",
    templateUrl: 'model-driven-form.html'
})
export class ModelDrivenForm {
    form: FormGroup;
    
    firstName = new FormControl("", Validators.required);
    
    constructor(fb: FormBuilder) {
        this.form = fb.group({
            "firstName": this.firstName,
            "password":["", Validators.required]
        });
    }
    onSubmitModelBased() {
        console.log("model-based form submitted");
        console.log(this.form);
    }
}
````

We can see that the form is really just a FormControl, which keeps track of the global validity state. The controls themselves can either be instantiated individually or defined using a simplified array notation using the form builder.

In the array notation, the first element of the array is the initial value of the control, and the remaining elements are the control's validators. In this case both controls are made mandatory via the Validators.required built-in validator.

_**Note** that ngModel can still be used with reactive forms. It's just that the form value would be available in two different places: the view model and the FormGroup, which could potentially lead to some confusion._

**Due to this reason mixing ngModel with reactive forms is best avoided.**

#### Advantages and disadvantages

You are probably wondering what we gained here. On the surface there is already a big gain: We can now unit test the form validation logic.

We can do that just by instantiating the class, setting some values in the form controls and perform assertions against the form global valid state and the validity state of each control.

But this is just one possibility. The FormGroup and
FormControl classes provide an API that allows us to build UIs using a completely different programming style known as Functional Reactive Programming.

## SASS

We use **SASS** as a preprocessor with the **SCSS syntax** to write our css. For more information check our [css style guide](../css/css.md).

## State Management

State is a bug! This means we want to avoid it as much as possible, however sometimes we need statemanagement either way.

For larger Angular applications with a lot of asynchronous activity and where there's a lot of state that is being shared and manipulated across multiple components and modules, managing state can be quite challenging. In a typical application, we're managing things like:

- Data that comes from the server and whether it's pending or resulted in an error
- UI state like toggles, alerts and errors messages
- User input, such as form submissions, filters and search queries
- Custom themes, credentials and localization
- Many other types of state

As the application grows, how do we know that a state change in one module will consistently and accurately reflected in other modules? And what if these modifications result in even more state changes? Eventually, it becomes extremely difficult to reason about what's actually happening in your application, and be a large source of bugs.

In Angular, there are **4 main ways** to solve this problem.

- Redux using @ngrx;
- Redux using ng2-redux; and
- Angular Services and RxJS.
- URL

We opt to choose for Redux and @ngrx. It's lightweight, fairly easy to setup en to use, and mostly used across the interwebs.

### Redux and @ngrx
#### What's Redux
Redux is an application state manager for JavaScript applications, and keeps with the core principles of the Flux-architecture by having a unidirectional data flow in your application.

Where Flux applications traditionally have multiple stores, Redux applications have only one global, read-only application state. This state is calculated by "reducing" over a collection or stream of actions that update it in controlled ways.
#### What's @ngrx
Redux state managers have been very well received and have inspired the creation of @ngrx, a set of modules that implement the same way of managing state as well as some of the middleware and tools in the Redux ecosystem. @ngrx was created to be used specifically with Angular and RxJS, as it leans heavily on the observable paradigm.

#### Setup
install via npm: 
`npm install @ngrx/core @ngrx/store --save`

#### Defining app state
When building an application using Redux, the first thing to think about is, "What state do I want to store?" It is generally a good idea to capture all of the application's state so that it can be accessible from anywhere and all in one place for easy inspection.

In the application state, we store things like:

- Data received through API calls
- User input
- Presentation state, such as menu and button toggles
- Application preferences
- Internationalization messages
- Themes and other customizable areas of your application

To define your application state, use an interface called AppState.

For example our `app/types/appState.ts`:
```typescript
export interface AppState {
  readonly colors: Colors;
  readonly localization: Localization;
  readonly login: Login;
  readonly projectList: ProjectList;
  readonly registration: Registration;
  readonly showMainNavigation: boolean;
}
```

_**Note**: We're using readonly to ensure compile-time immutability, and it provides the simplest immutable implementation without adding more dependencies to clutter the examples._

An example can be found [here](https://angular-2-training-book.rangle.io/handout/state-management/ngrx/example_application.html).

## Testing

### Unit testing

The objective of Unit Testing is to isolate a section of code and verify its correctness. In procedural programming a unit may be an individual function or procedure

The goal of Unit Testing is to isolate each part of the program and show that the individual parts are correct. Unit Testing is usually performed by the developer. 

Sometimes software developers attempt to save time by doing minimal unit testing. This is a myth because skimping on unit testing leads to higher Defect fixing costs during System Testing, Integration Testing and even Beta Testing after the application is completed. Proper unit testing done during the development stage saves both time and money in the end.

#### Jest

Jest is used by Facebook to test all JavaScript code including React applications. One of Jest's philosophies is to provide an integrated "zero-configuration" experience. We observed that when engineers are provided with ready-to-use tools, they end up writing more tests, which in turn results in more stable and healthy code bases.

Easily create code coverage reports using ```--coverage```. No additional setup or libraries needed! Jest can collect code coverage information from entire projects, including untested files.

##### Installation

- Install Jest using npm:

  ``npm install --save-dev jest @types/jest jest-preset-angular jest-zone-patch ts-jest``
  
###### package.json
Add this to your package.json file
````json
    "jest": {
        "preset": "jest-preset-angular",
        "setupTestFrameworkScriptFile": "<rootDir>/jest/setupJest.ts",
        "coverageReporters": [
            "lcov"
        ]
    }
````

and change the test scripts to: 
````json
    "scripts": {
        ...
        "test": "jest --coverage --watch",
        "test-once": "jest --coverage",
        "test-ci": "jest --coverage --bail",
        ...
    }
````
###### jest folder in root dir
Create a ``setupJest.ts`` file in the jest folder inside the root directory with all needed imports. For example:
````typescript
import 'jest-preset-angular';

import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/shareReplay';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/share';

import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/timer';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/proxy.js';
import 'zone.js/dist/sync-test';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';

import './jestGlobalMocks';
````

inside the same folder you can create a ``jestGlobalMocks.ts`` file with all global mocks, but most importantly following createMockObj function in order to create mock objects similar to Karma's way of testing.
````typescript
import Mock = jest.Mock;

export const createMockObj = (baseName, methodNames): { [key: string]: Mock<any> } => {
    const obj: any = {};

    for (let i = 0; i < methodNames.length; i++) {
        obj[methodNames[i]] = jest.fn();
    }

    return obj;
};
````

This way you can create mock objects inside your testing files. First of you need to import the function. For example a ``testfile.spec.ts``:
````typescript
import { createMockObj } from '../../../../jest/jestGlobalMocks';
...
beforeEach(() => {
        mockPlatform = createMockObj('platform', ['getPlatform']);
    });
...
````

###### Documentation
For more information about Jest and how it works be sure to read their [documentation](https://facebook.github.io/jest/docs/en/getting-started.html).

### Integration testing

Integration testing is a level of software testing where individual units are combined and tested as a group. The purpose of this level of testing is to expose faults in the interaction between integrated units. Test drivers and test stubs are used to assist in Integration Testing.
In our projects we'll use protractor with cucumber

#### Protractor
Protractor is an end-to-end test framework for Angular and AngularJS applications. Protractor runs tests against your application running in a real browser, interacting with it as a user would. 

#### Cucumber
Cucumber merges specification and test documentation into one cohesive whole. Business and IT don't always understand each other. Cucumber's executable specifications encourage closer collaboration, helping teams keep the business goal in mind at all times.


##### Installation
Use npm to install:

``npm install -g protractor``

And:

``npm install --save-dev protractor cucumber cucumber-html-reporter protractor-cucumber-framework``

_**Note**: if not already installed, please install Chai as well:_

`npm install --save-dev chai chai-is-promised `

This will install multiple line tools, protractor and webdriver-manager are on of these. Try running ``protractor --version`` to make sure it's working.

The webdriver-manager is a helper tool to easily get an instance of a Selenium Server running. Use it to download the necessary binaries with:

``webdriver-manager update``

Now start up a server with:

``webdriver-manager start``

This will start up a Selenium Server and will output a bunch of info logs. Your Protractor test will send requests to this server to control a local browser. You can see information about the status of the server at [http://localhost:4444/wd/hub](http://localhost:4444/wd/hub).

###### Configuration

In order to get these to work we need to make some changes to our package.json and jenkins config file:

####### package.json
````json
"setup-integration": "tsc -p ./integration/tsconfig.integration.json",
   "protractor": "protractor ./integration/compiled/protractor.conf.js",
   "protractor-ci": "protractor ./integration/compiled/protractor-jenkins.conf.js",
   "integration": "yarn setup-integration && yarn protractor",
   "integration-ci": "yarn setup-integration && yarn protractor-ci",
   "webdriver-update": "webdriver-manager update"
````

####### jenkins config
Add an extra build step to your Jenkins file
````jenkins
private void runIntegrationTests() {
   stage('Int. test') {
       gitlabCommitStatus('Integration tests') {
           container('node-chrome') {
               sh '''
                   yes | cp -rf config/env.integration.json dist/assets/environment.json
                   cat dist/assets/environment.json
                   node integration/server_backend.js &
                   node integration/server_frontend.js &
                   sleep 5
                   intTestFailed=1
                   yarn integration-ci && intTestFailed=0
               '''

               def intTestFailed = sh (
                   script: 'echo $intTestFailed',
                   returnStdout: true
               ).trim()

               if (intTestFailed == "1") {
                   error('Integration tests failed')
               }
           }
       }
   }
}
````


##### Usage

Integration tests run outside of our src/ folder and in our root dir. So before we start we make sure we have a e2e folder with the following content.
```markdown
├ integration/          
  ├ features/       
  ├ pages/       
  ├ stepdefinitions/       
  ├ support/       
  ├ testdata/
  ├ protractor-jenkins.conf.ts                 
  ├ protractor.conf.ts                 
  ├ server_backend.js                 
  ├ server_frontend.js                 
  └ tsconfig.integration.json       
```

- features
    - Place your cucumber feature files in this folder
    - Multiple scenarios for 1 feature
    - Minimum 3 steps (Given, When, Then)
- pages
    - Place your page objects files in this folder 
    - Define the elements used in the html element (ElementFinder)
    - Define actions that can happen on this page / html element (Functions)
- stepdefinitions
    - Place your stepdefinitions files in this folder
    - describes the steps used in the feature file
- Support
    - Necessary files to setup cucumber in this folder
    - Reporter.js generates html and json files for all features / scenarios
    - Hooks.js clear cache after each test + add console logs
- testdata
    - Place your mock json data files in this folder
    - Used by the server_backend.js file to serve these data as responses


###### portractor.conf.ts
This is our configuration file for protractor itself. A similar file could look somehting like this:
````typescript
import { Reporter } from './support/reporter';
import { Config, browser } from 'protractor';

const jsonReports = process.cwd() + '/integration/reports/json';

export const config: Config = {
 allScriptsTimeout: 11000,
 specs: ['../**/*.feature'],
 capabilities: {
   browserName: 'chrome'
 },
 directConnect: true,
 baseUrl: 'http://localhost:4200/',

 // Use a custom framework adapter and set its relative path
 framework: 'custom',
 frameworkPath: require.resolve('protractor-cucumber-framework'),

 beforeLaunch: function() {
   require('ts-node').register({ project: 'integration/tsconfig.integration.json' });
 },

 onPrepare: () => {
   browser.ignoreSynchronization = true;
   browser
     .manage()
     .window()
     .maximize();
   Reporter.createDirectory(jsonReports);
 },

 cucumberOpts: {
   compiler: 'ts:ts-node/register',
   format: 'json:./integration/reports/json/cucumber_report.json',
   require: ['../stepdefinitions/*.ts', '../support/*.ts'],
   strict: true
 },

 onComplete: () => {
   Reporter.createHTMLReport();
 }
};
````

###### protractor-jenkins.conf.ts
We added extra chomeOptions (no-sandbox, headless, ... ).
On prepare we are given the browser a fixed defined width & height
````typescript
import { Reporter } from './support/reporter';
import { Config, browser } from 'protractor';

const jsonReports = process.cwd() + '/integration/reports/json';

export const config: Config = {
 allScriptsTimeout: 11000,
 specs: ['../**/*.feature'],
 capabilities: {
   browserName: 'chrome',
   chromeOptions: {
     args: ['--no-sandbox', '--headless', '--disable-gpu', '--window-size=1920,1080']
   }
 },
 directConnect: true,
 baseUrl: 'http://localhost:4200/',

 // Use a custom framework adapter and set its relative path
 framework: 'custom',
 frameworkPath: require.resolve('protractor-cucumber-framework'),

 beforeLaunch: function() {
   require('ts-node').register({ project: 'integration/tsconfig.integration.json' });
 },

 onPrepare: () => {
   browser.ignoreSynchronization = true;
   browser
     .manage()
     .window()
     .setSize(1920, 1080);
   Reporter.createDirectory(jsonReports);
 },

 cucumberOpts: {
   compiler: 'ts:ts-node/register',
   format: 'json:./integration/reports/json/cucumber_report.json',
   require: ['../stepdefinitions/*.ts', '../support/*.ts'],
   strict: true
 },

 onComplete: () => {
   Reporter.createHTMLReport();
 }
};
````

###### server_frontend.js
`````js
var express = require('express');
var frontend = express();
var path = require('path');

frontend.listen(4200, function() {
 console.log('Started node server on port 4200');
});
//Serve dist folder
frontend.use(express.static(path.join(__dirname, '..', 'dist')));
console.log('__dirname = ' + __dirname);
frontend.use((req, res) => res.sendFile(path.join(__dirname, '..', 'dist', 'index.html')));
`````

###### server_backend.js
````js
var express = require('express');
var backend = express();
var fs = require('fs');
var bodyParser = require('body-parser');

var signIn = [];
var unauthorized = [];

loadInitialData();

backend.listen(8081, function() {
 console.log('Started node server on port 8081');
});

backend.use(bodyParser.json({ type: 'application/json' }));
backend.use(function(req, res, next) {
 res.header('Access-Control-Allow-Origin', '*');
 res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
 res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, PATCH, DELETE, OPTIONS');
 next();
});

function loadInitialData() {
 var signInData = fs.readFileSync('./integration/testdata/sign-in.json', 'utf8');
 var unauthorizedData = fs.readFileSync('./integration/testdata/unauthorized.json', 'utf8');
 signIn = JSON.parse(signInData);
 unauthorized = JSON.parse(unauthorizedData);
}

// example
backend.post('/login', function(req, res) {
 const username = req.body.username;
 const password = req.body.password;
 username == 'test' && password == 'test123' ? res.status(200).json(signIn) : res.status(401).json(unauthorized);
});
````

###### tsconfig.integration.json
````json
{
 "extends": "../tsconfig.json",
 "compilerOptions": {
   "sourceMap": false,
   "outDir": "./compiled",
   "module": "commonjs",
   "target": "es5",
   "types":[
     "node"
   ]
 },
 "files": [
   "protractor.conf.ts"
 ],
 "exclude": [],
 "include": [
   "../integration/*"
 ]
}

````

##### Features
These describe the actions a user has to do before completing a task. There can be multiple scenario's for a feature. Each with a given, when and then.

_**Note**: There can be more than 1 given!_

For example `authentication.feature` :
````feature
@SignInFeature
Feature: Sign in to the configurator

 Scenario: Valid login scenario
   Given I am on the authetication page
   When I sign in with valid credentials
   Then The secure page should be displayed

 Scenario: Invalid login scenario
   Given I am on the authetication page
   When I sign in with invalid credentials
   Then I should see a message that my credentials are invalid
````


##### Pages
Pageobjects are basically our functions and methods which we gonne invoke in our spec files. So in here you make a pageobject for each page/function/object/etc you want to test. Naming always ends with ```.po.ts```
To make everything a bit easier we can extend our PO's with a base.po.ts with some basic functions like clicking a button, etc. This file looks something like this:
``base.po.ts``
```typescript
import { browser, element, by, ElementFinder, protractor } from 'protractor';
import { Promise } from 'es6-promise';

const EC = protractor.ExpectedConditions;

export class BasePage {
    static TIMEOUT = 2000;

    pageElement: ElementFinder;
    popupDialog;

    constructor(pageElement: ElementFinder) {
        this.pageElement = pageElement;
        this.popupDialog = element(by.tagName('popups')).element(by.className('popup'));
    }

    navigateTo() {
        return browser.get('/');
    }

    isPresent() {
        return this.pageElement.isPresent();
    }

    isDisplayed() {
        return this.pageElement.isDisplayed();
    }

     waitForPresenceOfPage(timeout = BasePage.TIMEOUT) {
       return browser.wait(EC.presenceOf(this.pageElement), timeout);
     }
    
     waitForPresenceOf(elementFinder: ElementFinder, timeout = BasePage.TIMEOUT) {
       return browser.wait(EC.presenceOf(elementFinder), timeout);
     }
    
     waitForVisibliltyOf(elementFinder: ElementFinder, timeout = BasePage.TIMEOUT) {
       return browser.wait(EC.visibilityOf(elementFinder), timeout);
     }
    
     waitForVisibliltyAndClick(elementFinder: ElementFinder, timeout = BasePage.TIMEOUT) {
       return browser.wait(EC.visibilityOf(elementFinder), timeout).then(() => elementFinder.click());
     }

    clearStorage() {
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    }

    clickButton(button: ElementFinder, buttonName?: string): Promise<any> {
        console.log('Clicking ' + button.locator() + ' (' + (buttonName ? buttonName : '') + ')');
        return browser.wait(this.buttonIsClicked(button), BasePage.TIMEOUT)
            .then(() => {
            })
            .catch((err) => {
                fail('Button ' + button.locator() + ' not clicked within timemout.');
            });
    }

    buttonIsClicked(button: ElementFinder): any {
        return () => {
            return button.click()
                .then(() =>  true)
                .catch(() => false);
        };
    }

    selectOption(select: ElementFinder, index: number) {
        const options = select.element(by.className('options-wrapper')).all(by.tagName('li'));
        this.clickButton(options.get(index));
    }

    selectCheckbox(select: ElementFinder, index: number) {
        const options = select.element(by.className('options-wrapper')).all(by.tagName('checkbox'));
        this.clickButton(options.get(index));
    }

    toggleSelectBox(select: ElementFinder) {
        this.clickButton(select);
    }

    hasClass(el, cls) {
        return el.getAttribute('class').then(function (classes) {
            return classes.split(' ').indexOf(cls) !== -1;
        });
    }
}

```

In our constructor our components are all found under the **superelement**. This way only elements inside this superelement can be invoked. If for some reason you want to acces an element outside of this supereleme`nt you just leave out the ``.pageElement`` in the constructor for that element.

a simple `authentication.po.ts` then could look something like this:
````typescript
import { browser, element, by, ElementFinder } from 'protractor';
import { BasePage } from '../../pages/common/base.po';

export class AuthenticationPage extends BasePage {
 usernameField: ElementFinder;
 passwordField: ElementFinder;
 signInButton: ElementFinder;
 errorMessage: ElementFinder;

 constructor() {
   super(element(by.tagName('tvh-authentication')));
   this.usernameField = this.pageElement.element(by.tagName('form-textbox')).element(by.tagName('input'));
   this.passwordField = this.pageElement.element(by.tagName('form-password')).element(by.tagName('input'));
   this.signInButton = this.pageElement.element(by.tagName('button')).element(by.className('btn'));
   this.errorMessage = this.pageElement.element(by.className('alert-danger'));
 }

 navigateTo() {
   return browser.get('/authentication');
 }

 fillInUsernameField(login: string) {
   return this.usernameField.clear().then(() => {
     return this.usernameField.sendKeys(login);
   });
 }

 fillInPasswordField(password: string) {
   return this.passwordField.clear().then(() => this.passwordField.sendKeys(password));
 }

 clickSignIn() {
   return this.signInButton.click();
 }
}

````

Notice that the clickButton is a function from our ``base.po.ts file.``

##### Stepdefinitions
Spec files are our actual test files. In these files the steps of our test are defined. Although all test files must be focused on testing on aspect of our application. This doesn't mean that we need another spec file for every pageobject. We can have more pageobjects than specs.
A spec file could look something like this:
````typescript
import { AuthenticationPage } from '../pages/application/authentication.po';
import { SecurePage } from '../pages/application/secure.po';

const { Given, When, Then } = require('cucumber');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

const authenticationPage = new AuthenticationPage();
const securePage = new SecurePage();

Given(/^I am on the authetication page$/, async () => {
 await authenticationPage.navigateTo();
 await authenticationPage.waitForPresenceOfPage();
});

When(/^I sign in with valid credentials$/, async () => {
 await authenticationPage.fillInUsernameField('test');
 await authenticationPage.fillInPasswordField('test123');
 await authenticationPage.clickSignIn();
});

When(/^I sign in with invalid credentials$/, async () => {
 await authenticationPage.fillInUsernameField('wronguser');
 await authenticationPage.fillInPasswordField('wrongpassword');
 await authenticationPage.clickSignIn();
});

Then(/^The secure page should be displayed$/, async () => {
 await securePage.waitForPresenceOfPage();
 await expect(securePage.isPresent()).to.eventually.equal(true);
});

Then(/^I should see a message that my credentials are invalid$/, async () => {
 await authenticationPage.waitForPresenceOf(authenticationPage.errorMessage);
 await expect(authenticationPage.errorMessage.getText()).to.eventually.equal(
   'Wrong username or password. Please retry.'
 );
});
````

Notice that we acces multiple pageobject functions. 

##### Support
Here we define our hooks:
````typescript
import { browser } from 'protractor';
import { config } from '../protractor-jenkins.conf';

const { BeforeAll, After, Status } = require('cucumber');

BeforeAll({ timeout: 15 * 1000 }, async () => {
 await browser.get(config.baseUrl);
});

After(async function(scenario) {
 await browser.executeScript('window.localStorage.clear();');
 await browser.executeScript('window.sessionStorage.clear();');

 if (scenario.result.status === Status.FAILED) {
   await browser
     .manage()
     .logs()
     .get('browser')
     .then(function(browserLog) {
       console.log('\n\r############# START CONSOLE OUTPUT ##############');
       console.log('log: ' + require('util').inspect(browserLog));
       console.log('############## END CONSOLE OUTPUT ###############\n\r');
     });
   const screenShot = await browser.takeScreenshot();
   this.attach(screenShot, 'image/png');
 }
});
````

##### Reporter
````typescript
import * as reporter from 'cucumber-html-reporter';
import * as fs from 'fs';
import * as mkdirp from 'mkdirp';
import * as path from 'path';
const jsonReports = path.join(process.cwd(), '/integration/reports/json');
const htmlReports = path.join(process.cwd(), '/integration/reports/html');
const targetJson = jsonReports + '/cucumber_report.json';
const targetHtml = htmlReports + '/cucumber_report.html';

const cucumberReporterOptions = {
 jsonFile: targetJson,
 output: targetHtml,
 reportSuiteAsScenarios: true,
 theme: 'bootstrap'
};

export class Reporter {
 public static createDirectory(dir: string) {
   if (!fs.existsSync(dir)) {
     mkdirp.sync(dir);
   }
 }

 public static createHTMLReport() {
   try {
     reporter.generate(cucumberReporterOptions); // invoke cucumber-html-reporter
   } catch (err) {
     if (err) {
       throw new Error('Failed to save cucumber test results to json file.');
     }
   }
 }
}
````

##### Testdata
Mock data inside `.json` files, used as a response as if it was a real backend.
 
 
 

## tsconfig.json

TypeScript is a primary language for Angular application development. It is a superset of JavaScript with design-time support for type safety and tooling.

Browsers can't execute TypeScript directly. Typescript must be "transpiled" into JavaScript using the tsc compiler, which requires some configuration.

```json
{
    "compileOnSave": false,
    "compilerOptions": {
        "baseUrl": "src",
        "outDir": "./dist/out-tsc",
        "sourceMap": true,
        "declaration": false,
        "moduleResolution": "node",
        "emitDecoratorMetadata": true,
        "experimentalDecorators": true,
        "strictNullChecks": true,
        "noImplicitAny": true,
        "suppressImplicitAnyIndexErrors":true,
        "target": "es5",
        "typeRoots": [
            "node_modules/@types"
        ],
        "lib": [
            "es2017",
            "dom"
        ]
    }
}
```
- Note that we set ``NoImplicitAny`` on **true**. When the noImplicitAny flag is true and the TypeScript compiler cannot infer the type, it still generates the JavaScript files, but it also reports an error. This is a stricter setting but it catches more unintentional errors at compile time.
  - *You can still sat a variable's type to any even when this setting is set on true.*
  - When ``NoImplicityAny`` is set on true we might get implicit index errors as well. We don't want these hence we add  ````"suppressImplicitAnyIndexErrors":true````. This line suppresses these errors.
- By default ```null``` and `undefined` are assignable to all types in TypeScript. 

````typescript
let foo: number = 123;
foo = null; // OK
foo = undefined; // OK
````
  
  In strict null checking mode, these two are different
  
````typescript
let foo = undefined;
foo = null; // NOT Okay
````
  
  Undefined is the root of all evil. It often leads to runtime errors. It is easy to write code that will throw ``Error`` at runtime. For example we have this ```Member``` interface:
  
````typescript
interface Member {
  name: string,
  age?: number
}
````
  
  Age is optional, meaning the value may or may not be ```undefined```
  If we run the folleowing code we'll get a runtime error.
  
````typescript
getMember()
  .then(member: Member => {
    const stringifyAge = member.age.toString() // Cannot read property 'toString' of undefined
  })
````
  
  However with ```StrictNullChecks``` on true this error will be caught at compile time.
  
````typescript
getMember()
  .then(member: Member => {
    const stringifyAge = member.age.toString() // Object is possibly 'undefined'
  })
````
 
