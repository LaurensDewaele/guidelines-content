---
title: Folder Structure
menu:
  foundation:
    title: "Folder Structure"
    parent: "Angular"
weight: 1
---

Folder structure is very important in order to keep a good overview and knowledge of which file is located where, and how many responsibility it has. Keep in mind we want to have as many dumb components as possible.

The main structure in our /src folder is gonna look something like this.

```markdown
├ app/          : The main files for our application
├ assets/       : The location for our fonts, i18n, images, js, ...
├ common/       : Components, directives, guards, utils, ... that can be reused across our application
├ modules/      : Our major modules grouped together
├ styles/       : Our styling
├ index.html    : The main .html file
└ main.ts       : The main .ts file
```

## General Component Structure
In order to keep every component structured, components are sliced into several components. Each presentational module subscribes, through its own sandbox, to events published by the application core. This is an example of a module in our modules folder.

```markdown
├ module1/            : The main files for our application
  ├ components/          : All components
  ├ containers/          : The containers
  ├ services/            : Services for backend calls
  ├ component1.sandbox   : Sandbox component 
  └ index.ts             : The main .ts file for our declarations, imports, etc.
```

## App Folder
The app folder contains all main app component files. These include the routing, navigation, notification, popups, etc. A structure could look something like this.
```markdown
├ app/                              
  ├ components/                    
  | └ navbar/                       
  |   ├ navbar.component.html      
  |   ├ navbar.component.scss       
  |   └ navbar.component.ts         
  ├ containers/                     
  | ├ application/                  
  | └ root/                         
  ├ controllers/                   
  | ├ notifications/                
  | └ popups/                       
  ├ services/                       
  | ├ service1.ts                  
  | └ service2.ts                  
  ├ component1.sandbox              
  └ index.ts                       
```
*Note:* This is just an example, controllers or other components might not be necessary according to your application.

## Assets Folder
The assets folder contains files such as fonts being used, images, translation and the environment(s) as well.
```markdown
├ assets/                              
  ├ fonts/                    
  | ├ fontawesome/  
  | | ├ font.otf     
  | | ├ font.woff      
  | | └ font.ttf                                                            
  ├ i18n/                     
  | ├ en.json                 
  | ├ nl.json                 
  | └ ...                       
  ├ img/                   
  | ├ cars/                
  | └ team.png                       
  ├ js/                       
  | ├ splash.js                  
  | └ ... .js                              
  └ env.js                       
```
Note that there is no index.ts file in the assets folder. Our environment can be set here as well.

## Common Folder
The common folder holds all files that can be used across the entire application. This is also the place where to put pipes, directives and utils.
```markdown
├ common/                              
  ├ components/                                                
  ├ containers/                                           
  ├ directives/                   
  | ├ color/                
  | ├ color-picker/                
  | └ ...               
  ├ guards/                                                
  ├ pipes/                                           
  ├ services/                              
  ├ utils/                              
  ├ types/                :Our models                            
  └ index.ts                       
```
## Modules Folder
Each major module consists out of 4 major block, the components, containers, services and the sanbox.
```markdown
├ module1/                              
  ├ components/   
  | ├ list-item/                
  | | ├ list-item.component.html      
  | | ├ list-item.component.scss       
  | | ├ list-item.component.spec.ts       
  | | └ list-item.component.ts                
  | ├ detail-item/                
  | | ├ detail-item.component.html      
  | | ├ detail-item.component.scss       
  | | ├ detail-item.component.spec.ts       
  | | └ detail-item.component.ts                                             
  ├ containers/               
  | └ component1-container/     
  | | ├ component1.container.html      
  | | ├ component1.container.scss       
  | | ├ component1.container.spec.ts       
  | | └ component1.container.ts                                   
  ├ services/     
  | ├  service1.service.ts   
  | └ service1.service.spec.ts   
  ├ component1.sanbox.ts                                                                         
  ├ component1.sanbox.spec.ts                                                                         
  └ index.ts                       
```

## Styles Folder
This is our base folder structure for our styling. For more information check our [css style guide](../css/).
