---
title: tsconfig.json
menu:
  foundation:
    title: "tsconfig.json"
    parent: "Angular"
weight: 7
---
TypeScript is a primary language for Angular application development. It is a superset of JavaScript with design-time support for type safety and tooling.

Browsers can't execute TypeScript directly. Typescript must be "transpiled" into JavaScript using the tsc compiler, which requires some configuration.

```json
{
    "compileOnSave": false,
    "compilerOptions": {
        "baseUrl": "src",
        "outDir": "./dist/out-tsc",
        "sourceMap": true,
        "declaration": false,
        "moduleResolution": "node",
        "emitDecoratorMetadata": true,
        "experimentalDecorators": true,
        "strictNullChecks": true,
        "noImplicitAny": true,
        "suppressImplicitAnyIndexErrors":true,
        "target": "es5",
        "typeRoots": [
            "node_modules/@types"
        ],
        "lib": [
            "es2017",
            "dom"
        ]
    }
}
```
- Note that we set ``NoImplicitAny`` on **true**. When the noImplicitAny flag is true and the TypeScript compiler cannot infer the type, it still generates the JavaScript files, but it also reports an error. This is a stricter setting but it catches more unintentional errors at compile time.
  - *You can still sat a variable's type to any even when this setting is set on true.*
  - When ``NoImplicityAny`` is set on true we might get implicit index errors as well. We don't want these hence we add  ````"suppressImplicitAnyIndexErrors":true````. This line suppresses these errors.
- By default ```null``` and `undefined` are assignable to all types in TypeScript. 

````typescript
let foo: number = 123;
foo = null; // OK
foo = undefined; // OK
````
  
  In strict null checking mode, these two are different
  
````typescript
let foo = undefined;
foo = null; // NOT Okay
````
  
  Undefined is the root of all evil. It often leads to runtime errors. It is easy to write code that will throw ``Error`` at runtime. For example we have this ```Member``` interface:
  
````typescript
interface Member {
  name: string,
  age?: number
}
````
  
  Age is optional, meaning the value may or may not be ```undefined```
  If we run the folleowing code we'll get a runtime error.
  
````typescript
getMember()
  .then(member: Member => {
    const stringifyAge = member.age.toString() // Cannot read property 'toString' of undefined
  })
````
  
  However with ```StrictNullChecks``` on true this error will be caught at compile time.
  
````typescript
getMember()
  .then(member: Member => {
    const stringifyAge = member.age.toString() // Object is possibly 'undefined'
  })
````
