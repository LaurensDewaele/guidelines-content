---
title: State Management
menu:
  foundation:
    title: "State Management"
    parent: "Angular"
weight: 5
---

State is a bug! This means we want to avoid it as much as possible, however sometimes we need statemanagement either way.

For larger Angular applications with a lot of asynchronous activity and where there's a lot of state that is being shared and manipulated across multiple components and modules, managing state can be quite challenging. In a typical application, we're managing things like:

- Data that comes from the server and whether it's pending or resulted in an error
- UI state like toggles, alerts and errors messages
- User input, such as form submissions, filters and search queries
- Custom themes, credentials and localization
- Many other types of state

As the application grows, how do we know that a state change in one module will consistently and accurately reflected in other modules? And what if these modifications result in even more state changes? Eventually, it becomes extremely difficult to reason about what's actually happening in your application, and be a large source of bugs.

In Angular, there are **4 main ways** to solve this problem.

- Redux using @ngrx;
- Redux using ng2-redux; and
- Angular Services and RxJS.
- URL

We opt to choose for Redux and @ngrx. It's lightweight, fairly easy to setup en to use, and mostly used across the interwebs.

## Redux and @ngrx
### What's Redux
Redux is an application state manager for JavaScript applications, and keeps with the core principles of the Flux-architecture by having a unidirectional data flow in your application.

Where Flux applications traditionally have multiple stores, Redux applications have only one global, read-only application state. This state is calculated by "reducing" over a collection or stream of actions that update it in controlled ways.
### What's @ngrx
Redux state managers have been very well received and have inspired the creation of @ngrx, a set of modules that implement the same way of managing state as well as some of the middleware and tools in the Redux ecosystem. @ngrx was created to be used specifically with Angular and RxJS, as it leans heavily on the observable paradigm.

### Setup
install via npm: 
`npm install @ngrx/core @ngrx/store --save`

### Defining app state
When building an application using Redux, the first thing to think about is, "What state do I want to store?" It is generally a good idea to capture all of the application's state so that it can be accessible from anywhere and all in one place for easy inspection.

In the application state, we store things like:

- Data received through API calls
- User input
- Presentation state, such as menu and button toggles
- Application preferences
- Internationalization messages
- Themes and other customizable areas of your application

To define your application state, use an interface called AppState.

For example our `app/types/appState.ts`:
```typescript
export interface AppState {
  readonly colors: Colors;
  readonly localization: Localization;
  readonly login: Login;
  readonly projectList: ProjectList;
  readonly registration: Registration;
  readonly showMainNavigation: boolean;
}
```

_**Note**: We're using readonly to ensure compile-time immutability, and it provides the simplest immutable implementation without adding more dependencies to clutter the examples._

An example can be found [here](https://angular-2-training-book.rangle.io/handout/state-management/ngrx/example_application.html).
