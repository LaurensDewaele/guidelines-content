---
title: Sass
menu:
  foundation:
    title: "Sass"
    parent: "Angular"
weight: 4
---

We use **SASS** as a preprocessor with the **SCSS syntax** to write our css. For more information check our [css style guide](../../css/).