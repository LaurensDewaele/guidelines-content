---
title: Forms
menu:
  foundation:
    title: "Forms"
    parent: "Angular"
weight: 3
---

If a Vue component has so many props (or listeners, directives, ...) that they don't fit on one line anymore you need to put every prop on it's own line. Every line needs to be intended with 2 spaces. The closing > goes on a new unintended line followed by the closing tag.


````html
<!-- Good -->
<template>
  <my-component myProp="value"><my-component>
</template>
````
`````html
<!-- Good -->
<template>
  <my-component
    v-if="shouldDisplay"
    myProp="value"
    @change="handleEvent"
  ></my-component>
</template>

<!-- Bad: wrong indentation, closing `>` is not correct placed -->
<template>
  <my-component
    v-if="shouldDisplay"
    myProp="value"
    @change="handleEvent">
  </my-component>
</template>
`````

## Reactive forms vs Template Driven

Template Driven Forms are maybe for simple forms slightly less verbose, but the difference is not significant. Reactive Forms are actually much more powerful and have a nearly equivalent readability.

Most likely in a large-scale application, we will end up needing the functionality of reactive driven forms for implementing more advanced use cases like for example auto-save.

For this reason we'll opt to use reactive forms over template driven ones.

## Template driven forms

Angular provides form-specific directives that you can use to bind the form input data and the model. The form-specific directives add extra functionality and behavior to a plain HTML form. The end result is that the template takes care of binding values with the model and form validation. 

A short example looks something like this

````html
<!-- app.component.html -->

<div class="container">
  <h1>Add user</h1>
  <form #f="ngForm" novalidate (ngSubmit)="save(f.value, f.valid)">
    <div class="form-group">
      <label>Name</label>
      <input type="text" class="form-control" 
        name="name" [(ngModel)]="user.name" 
        #name="ngModel" required minlength="5">
      <small [hidden]="name.valid || (name.pristine && !f.submitted)" class="text-danger">
            Name is required (minimum 5 characters).
          </small>
      <!--<pre>{{name.errors | json}}</pre>-->
    </div>
    <div ngModelGroup="address">
      <div class="form-group">
        <label>Street</label>
        <input type="text" class="form-control" 
          name="street" [(ngModel)]="user.address.street"
          #street="ngModel" required>
        <small [hidden]="street.valid || (street.pristine && !f.submitted)" class="text-danger">
          Street is required.
        </small>
      </div>
      <div class="form-group">
        <label>Post code</label>
        <input type="text" class="form-control" 
          name="postcode" [(ngModel)]="user.address.postcode">
      </div>
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
  </form>
  <div class="margin-20">
    <div>Form details:-</div>
    <pre>Is form valid?: <br>{{f.valid | json}}</pre>
    <pre>Is form submitted?: <br>{{f.submitted | json}}</pre>
    <pre>form value: <br>{{f.value | json}}</pre>
  </div>
</div>
````

And our typescript:
````typescript
// app.component.ts
import { Component, OnInit } from '@angular/core';
import { User } from './user.interface';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
  public user: User;
  
  ngOnInit() {
    this.user = {
      name: '',
        address: {
         street: '',
         postcode: '8000'
      }
    };
  }
  
  save(model: User, isValid: boolean) {
    console.log(model, isValid);
  }
}
````

## Reactive forms
Reactive forms use a `formgroup` directory applied to the whole form. Validators are also placed in the controller instead of in the html itself.

For example:
````html
<section class="sample-app-content">
    <h1>Model-based Form Example:</h1>
    <form [formGroup]="form" (ngSubmit)="onSubmit()">
        <p>
            <label>First Name:</label>
            <input type="text" formControlName="firstName">
        </p>
        <p>
            <label>Password:</label>
            <input type="password" formControlName="password">
        </p>
        <p>
            <button type="submit" [disabled]="!form.valid">Submit</button>
        </p>
    </form>
</section>
````
and our component:
````typescript
import { FormGroup, FormControl, Validators, FormBuilder } 
    from '@angular/forms';

@Component({
    selector: "model-driven-form",
    templateUrl: 'model-driven-form.html'
})
export class ModelDrivenForm {
    form: FormGroup;
    
    firstName = new FormControl("", Validators.required);
    
    constructor(fb: FormBuilder) {
        this.form = fb.group({
            "firstName": this.firstName,
            "password":["", Validators.required]
        });
    }
    onSubmitModelBased() {
        console.log("model-based form submitted");
        console.log(this.form);
    }
}
````

We can see that the form is really just a FormControl, which keeps track of the global validity state. The controls themselves can either be instantiated individually or defined using a simplified array notation using the form builder.

In the array notation, the first element of the array is the initial value of the control, and the remaining elements are the control's validators. In this case both controls are made mandatory via the Validators.required built-in validator.

_**Note** that ngModel can still be used with reactive forms. It's just that the form value would be available in two different places: the view model and the FormGroup, which could potentially lead to some confusion._

**Due to this reason mixing ngModel with reactive forms is best avoided.**

### Advantages and disadvantages

You are probably wondering what we gained here. On the surface there is already a big gain: We can now unit test the form validation logic.

We can do that just by instantiating the class, setting some values in the form controls and perform assertions against the form global valid state and the validity state of each control.

But this is just one possibility. The FormGroup and
FormControl classes provide an API that allows us to build UIs using a completely different programming style known as Functional Reactive Programming.
