---
title: Testing
menu:
  foundation:
    title: "Testing"
    parent: "Angular"
weight: 6
---

## Unit testing

The objective of Unit Testing is to isolate a section of code and verify its correctness. In procedural programming a unit may be an individual function or procedure

The goal of Unit Testing is to isolate each part of the program and show that the individual parts are correct. Unit Testing is usually performed by the developer.

Sometimes software developers attempt to save time by doing minimal unit testing. This is a myth because skimping on unit testing leads to higher Defect fixing costs during System Testing, Integration Testing and even Beta Testing after the application is completed. Proper unit testing done during the development stage saves both time and money in the end.

### Jest

Jest is used by Facebook to test all JavaScript code including React applications. One of Jest's philosophies is to provide an integrated "zero-configuration" experience. We observed that when engineers are provided with ready-to-use tools, they end up writing more tests, which in turn results in more stable and healthy code bases.

Easily create code coverage reports using `--coverage`. No additional setup or libraries needed! Jest can collect code coverage information from entire projects, including untested files.

#### Installation

- Install Jest using npm:

  `npm install --save-dev jest @types/jest jest-preset-angular jest-zone-patch ts-jest`

##### package.json

Add this to your package.json file

```json
    "jest": {
        "preset": "jest-preset-angular",
        "setupTestFrameworkScriptFile": "<rootDir>/jest/setupJest.ts",
        "coverageReporters": [
            "lcov"
        ]
    }
```

For angular 6 use

```json
    "jest": {
        "preset": "jest-preset-angular",
        "setupTestFrameworkScriptFile": "<rootDir>/jest/setupJest.ts",
        "coverageReporters": [
            "lcov"
        ],
        "transformIgnorePatterns": [
          "node_modules/(?!@ngrx|angular2-ui-switch|ng-dynamic)"
        ]
    }
```

And edit tsconfig.spec.ts compileroptions

```json
  "compilerOptions": {
    "outDir": "../out-tsc/spec",
    "module": "commonjs",
    "types": [
      "jest",
      "jquery",
      "jsdom",
      "node"
    ]
  },
```

Change the test scripts to:

```json
    "scripts": {
        ...
        "test": "jest --coverage --watch",
        "test-once": "jest --coverage",
        "test-ci": "jest --coverage --bail",
        ...
    }
```

##### jest folder in root dir

Create a `setupJest.ts` file in the jest folder inside the root directory with all needed imports. For example:

```typescript
import "jest-preset-angular";

import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/shareReplay";
import "rxjs/add/operator/startWith";
import "rxjs/add/operator/distinctUntilChanged";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/skip";
import "rxjs/add/operator/take";
import "rxjs/add/operator/share";

import "rxjs/add/observable/combineLatest";
import "rxjs/add/observable/forkJoin";
import "rxjs/add/observable/timer";
import "rxjs/add/observable/throw";
import "rxjs/add/observable/of";

import "zone.js/dist/long-stack-trace-zone";
import "zone.js/dist/proxy.js";
import "zone.js/dist/sync-test";
import "zone.js/dist/async-test";
import "zone.js/dist/fake-async-test";

import "./jestGlobalMocks";
```

inside the same folder you can create a `jestGlobalMocks.ts` file with all global mocks, but most importantly following createMockObj function in order to create mock objects similar to Karma's way of testing.

```typescript
import Mock = jest.Mock;

export const createMockObj = (
  baseName,
  methodNames
): { [key: string]: Mock<any> } => {
  const obj: any = {};

  for (let i = 0; i < methodNames.length; i++) {
    obj[methodNames[i]] = jest.fn();
  }

  return obj;
};
```

This way you can create mock objects inside your testing files. First of you need to import the function. For example a `testfile.spec.ts`:

```typescript
import { createMockObj } from '../../../../jest/jestGlobalMocks';
...
beforeEach(() => {
        mockPlatform = createMockObj('platform', ['getPlatform']);
    });
...
```

##### Documentation

For more information about Jest and how it works be sure to read their [documentation](https://facebook.github.io/jest/docs/en/getting-started.html).

## Integration testing

Integration testing is a level of software testing where individual units are combined and tested as a group. The purpose of this level of testing is to expose faults in the interaction between integrated units. Test drivers and test stubs are used to assist in Integration Testing.
In our projects we'll use protractor with cucumber

### Protractor

Protractor is an end-to-end test framework for Angular and AngularJS applications. Protractor runs tests against your application running in a real browser, interacting with it as a user would.

### Cucumber

Cucumber merges specification and test documentation into one cohesive whole. Business and IT don't always understand each other. Cucumber's executable specifications encourage closer collaboration, helping teams keep the business goal in mind at all times.

#### Installation

Use npm to install:

`npm install -g protractor`

And:

`npm install --save-dev protractor cucumber cucumber-html-reporter protractor-cucumber-framework`

_**Note**: if not already installed, please install Chai as well:_

`npm install --save-dev chai chai-is-promised`

This will install multiple line tools, protractor and webdriver-manager are on of these. Try running `protractor --version` to make sure it's working.

The webdriver-manager is a helper tool to easily get an instance of a Selenium Server running. Use it to download the necessary binaries with:

`webdriver-manager update`

Now start up a server with:

`webdriver-manager start`

This will start up a Selenium Server and will output a bunch of info logs. Your Protractor test will send requests to this server to control a local browser. You can see information about the status of the server at [http://localhost:4444/wd/hub](http://localhost:4444/wd/hub).

##### Configuration

In order to get these to work we need to make some changes to our package.json and jenkins config file:

###### package.json

```json
"setup-integration": "tsc -p ./integration/tsconfig.integration.json",
   "protractor": "protractor ./integration/compiled/protractor.conf.js",
   "protractor-ci": "protractor ./integration/compiled/protractor-jenkins.conf.js",
   "integration": "npm webdriver-update && npm setup-integration && yarn protractor",
   "integration-ci": "npm setup-integration && npm protractor-ci",
   "webdriver-update": "webdriver-manager update"
```

###### jenkins config

Add an extra build step to your Jenkins file

```jenkins
private void runIntegrationTests() {
   stage('Int. test') {
       gitlabCommitStatus('Integration tests') {
           container('node-chrome') {
               sh '''
                   yes | cp -rf config/env.integration.json dist/assets/environment.json
                   cat dist/assets/environment.json
                   node integration/server_backend.js &
                   node integration/server_frontend.js &
                   sleep 5
                   intTestFailed=1
                   yarn integration-ci && intTestFailed=0
               '''

               def intTestFailed = sh (
                   script: 'echo $intTestFailed',
                   returnStdout: true
               ).trim()

               if (intTestFailed == "1") {
                   error('Integration tests failed')
               }
           }
       }
   }
}
```

#### Usage

Integration tests run outside of our src/ folder and in our root dir. So before we start we make sure we have a e2e folder with the following content.

```markdown
├ integration/  
 ├ features/  
 ├ pages/  
 ├ stepdefinitions/  
 ├ support/  
 ├ testdata/
├ protractor-jenkins.conf.ts  
 ├ protractor.conf.ts  
 ├ server_backend.js  
 ├ server_frontend.js  
 └ tsconfig.integration.json
```

- features
  - Place your cucumber feature files in this folder
  - Multiple scenarios for 1 feature
  - Minimum 3 steps (Given, When, Then)
- pages
  - Place your page objects files in this folder
  - Define the elements used in the html element (ElementFinder)
  - Define actions that can happen on this page / html element (Functions)
- stepdefinitions
  - Place your stepdefinitions files in this folder
  - describes the steps used in the feature file
- Support
  - Necessary files to setup cucumber in this folder
  - Reporter.js generates html and json files for all features / scenarios
  - Hooks.js clear cache after each test + add console logs
- testdata
  - Place your mock json data files in this folder
  - Used by the server_backend.js file to serve these data as responses

##### portractor.conf.ts

This is our configuration file for protractor itself. A similar file could look somehting like this:

```typescript
import { Reporter } from "./support/reporter";
import { Config, browser } from "protractor";

const jsonReports = process.cwd() + "/integration/reports/json";

export const config: Config = {
  allScriptsTimeout: 11000,
  specs: ["../**/*.feature"],
  capabilities: {
    browserName: "chrome"
  },
  directConnect: true,
  baseUrl: "http://localhost:4200/",

  // Use a custom framework adapter and set its relative path
  framework: "custom",
  frameworkPath: require.resolve("protractor-cucumber-framework"),

  beforeLaunch: function() {
    require("ts-node").register({
      project: "integration/tsconfig.integration.json"
    });
  },

  onPrepare: () => {
    browser.ignoreSynchronization = true;
    browser
      .manage()
      .window()
      .maximize();
    Reporter.createDirectory(jsonReports);
  },

  cucumberOpts: {
    compiler: "ts:ts-node/register",
    format: "json:./integration/reports/json/cucumber_report.json",
    require: ["../stepdefinitions/*.ts", "../support/*.ts"],
    strict: true
  },

  onComplete: () => {
    Reporter.createHTMLReport();
  }
};
```

##### protractor-jenkins.conf.ts

We added extra chomeOptions (no-sandbox, headless, ... ).
On prepare we are given the browser a fixed defined width & height

```typescript
import { Reporter } from "./support/reporter";
import { Config, browser } from "protractor";

const jsonReports = process.cwd() + "/integration/reports/json";

export const config: Config = {
  allScriptsTimeout: 11000,
  specs: ["../**/*.feature"],
  capabilities: {
    browserName: "chrome",
    chromeOptions: {
      args: [
        "--no-sandbox",
        "--headless",
        "--disable-gpu",
        "--window-size=1920,1080"
      ]
    }
  },
  directConnect: true,
  baseUrl: "http://localhost:4200/",

  // Use a custom framework adapter and set its relative path
  framework: "custom",
  frameworkPath: require.resolve("protractor-cucumber-framework"),

  beforeLaunch: function() {
    require("ts-node").register({
      project: "integration/tsconfig.integration.json"
    });
  },

  onPrepare: () => {
    browser.ignoreSynchronization = true;
    browser
      .manage()
      .window()
      .setSize(1920, 1080);
    Reporter.createDirectory(jsonReports);
  },

  cucumberOpts: {
    compiler: "ts:ts-node/register",
    format: "json:./integration/reports/json/cucumber_report.json",
    require: ["../stepdefinitions/*.ts", "../support/*.ts"],
    strict: true
  },

  onComplete: () => {
    Reporter.createHTMLReport();
  }
};
```

##### server_frontend.js

```js
var express = require("express");
var frontend = express();
var path = require("path");

frontend.listen(4200, function() {
  console.log("Started node server on port 4200");
});
//Serve dist folder
frontend.use(express.static(path.join(__dirname, "..", "dist")));
console.log("__dirname = " + __dirname);
frontend.use((req, res) =>
  res.sendFile(path.join(__dirname, "..", "dist", "index.html"))
);
```

##### server_backend.js

```js
var express = require("express");
var backend = express();
var fs = require("fs");
var bodyParser = require("body-parser");

var signIn = [];
var unauthorized = [];

loadInitialData();

backend.listen(8081, function() {
  console.log("Started node server on port 8081");
});

backend.use(bodyParser.json({ type: "application/json" }));
backend.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.header(
    "Access-Control-Allow-Methods",
    "POST, GET, PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});

function loadInitialData() {
  var signInData = fs.readFileSync(
    "./integration/testdata/sign-in.json",
    "utf8"
  );
  var unauthorizedData = fs.readFileSync(
    "./integration/testdata/unauthorized.json",
    "utf8"
  );
  signIn = JSON.parse(signInData);
  unauthorized = JSON.parse(unauthorizedData);
}

// example
backend.post("/login", function(req, res) {
  const username = req.body.username;
  const password = req.body.password;
  username == "test" && password == "test123"
    ? res.status(200).json(signIn)
    : res.status(401).json(unauthorized);
});
```

##### tsconfig.integration.json

```json
{
  "extends": "../tsconfig.json",
  "compilerOptions": {
    "sourceMap": false,
    "outDir": "./compiled",
    "module": "commonjs",
    "target": "es5",
    "types": ["node"]
  },
  "files": ["protractor.conf.ts"],
  "exclude": [],
  "include": ["../integration/*"]
}
```

#### Features

These describe the actions a user has to do before completing a task. There can be multiple scenario's for a feature. Each with a given, when and then.

_**Note**: There can be more than 1 given!_

For example `authentication.feature` :

```feature
@SignInFeature
Feature: Sign in to the configurator

 Scenario: Valid login scenario
   Given I am on the authetication page
   When I sign in with valid credentials
   Then The secure page should be displayed

 Scenario: Invalid login scenario
   Given I am on the authetication page
   When I sign in with invalid credentials
   Then I should see a message that my credentials are invalid
```

#### Pages

Pageobjects are basically our functions and methods which we gonne invoke in our spec files. So in here you make a pageobject for each page/function/object/etc you want to test. Naming always ends with `.po.ts`
To make everything a bit easier we can extend our PO's with a base.po.ts with some basic functions like clicking a button, etc. This file looks something like this:
`base.po.ts`

```typescript
import { browser, element, by, ElementFinder, protractor } from "protractor";
import { Promise } from "es6-promise";

const EC = protractor.ExpectedConditions;

export class BasePage {
  static TIMEOUT = 2000;

  pageElement: ElementFinder;
  popupDialog;

  constructor(pageElement: ElementFinder) {
    this.pageElement = pageElement;
    this.popupDialog = element(by.tagName("popups")).element(
      by.className("popup")
    );
  }

  navigateTo() {
    return browser.get("/");
  }

  isPresent() {
    return this.pageElement.isPresent();
  }

  isDisplayed() {
    return this.pageElement.isDisplayed();
  }

  waitForPresenceOfPage(timeout = BasePage.TIMEOUT) {
    return browser.wait(EC.presenceOf(this.pageElement), timeout);
  }

  waitForPresenceOf(elementFinder: ElementFinder, timeout = BasePage.TIMEOUT) {
    return browser.wait(EC.presenceOf(elementFinder), timeout);
  }

  waitForVisibliltyOf(
    elementFinder: ElementFinder,
    timeout = BasePage.TIMEOUT
  ) {
    return browser.wait(EC.visibilityOf(elementFinder), timeout);
  }

  waitForVisibliltyAndClick(
    elementFinder: ElementFinder,
    timeout = BasePage.TIMEOUT
  ) {
    return browser
      .wait(EC.visibilityOf(elementFinder), timeout)
      .then(() => elementFinder.click());
  }

  clearStorage() {
    browser.executeScript("window.sessionStorage.clear();");
    browser.executeScript("window.localStorage.clear();");
  }

  clickButton(button: ElementFinder, buttonName?: string): Promise<any> {
    console.log(
      "Clicking " +
        button.locator() +
        " (" +
        (buttonName ? buttonName : "") +
        ")"
    );
    return browser
      .wait(this.buttonIsClicked(button), BasePage.TIMEOUT)
      .then(() => {})
      .catch(err => {
        fail("Button " + button.locator() + " not clicked within timemout.");
      });
  }

  buttonIsClicked(button: ElementFinder): any {
    return () => {
      return button
        .click()
        .then(() => true)
        .catch(() => false);
    };
  }

  selectOption(select: ElementFinder, index: number) {
    const options = select
      .element(by.className("options-wrapper"))
      .all(by.tagName("li"));
    this.clickButton(options.get(index));
  }

  selectCheckbox(select: ElementFinder, index: number) {
    const options = select
      .element(by.className("options-wrapper"))
      .all(by.tagName("checkbox"));
    this.clickButton(options.get(index));
  }

  toggleSelectBox(select: ElementFinder) {
    this.clickButton(select);
  }

  hasClass(el, cls) {
    return el.getAttribute("class").then(function(classes) {
      return classes.split(" ").indexOf(cls) !== -1;
    });
  }
}
```

In our constructor our components are all found under the **superelement**. This way only elements inside this superelement can be invoked. If for some reason you want to acces an element outside of this supereleme`nt you just leave out the`.pageElement` in the constructor for that element.

a simple `authentication.po.ts` then could look something like this:

```typescript
import { browser, element, by, ElementFinder } from "protractor";
import { BasePage } from "../../pages/common/base.po";

export class AuthenticationPage extends BasePage {
  usernameField: ElementFinder;
  passwordField: ElementFinder;
  signInButton: ElementFinder;
  errorMessage: ElementFinder;

  constructor() {
    super(element(by.tagName("tvh-authentication")));
    this.usernameField = this.pageElement
      .element(by.tagName("form-textbox"))
      .element(by.tagName("input"));
    this.passwordField = this.pageElement
      .element(by.tagName("form-password"))
      .element(by.tagName("input"));
    this.signInButton = this.pageElement
      .element(by.tagName("button"))
      .element(by.className("btn"));
    this.errorMessage = this.pageElement.element(by.className("alert-danger"));
  }

  navigateTo() {
    return browser.get("/authentication");
  }

  fillInUsernameField(login: string) {
    return this.usernameField.clear().then(() => {
      return this.usernameField.sendKeys(login);
    });
  }

  fillInPasswordField(password: string) {
    return this.passwordField
      .clear()
      .then(() => this.passwordField.sendKeys(password));
  }

  clickSignIn() {
    return this.signInButton.click();
  }
}
```

Notice that the clickButton is a function from our `base.po.ts file.`

#### Stepdefinitions

Spec files are our actual test files. In these files the steps of our test are defined. Although all test files must be focused on testing on aspect of our application. This doesn't mean that we need another spec file for every pageobject. We can have more pageobjects than specs.
A spec file could look something like this:

```typescript
import { AuthenticationPage } from "../pages/application/authentication.po";
import { SecurePage } from "../pages/application/secure.po";

const { Given, When, Then } = require("cucumber");
const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;

const authenticationPage = new AuthenticationPage();
const securePage = new SecurePage();

Given(/^I am on the authetication page$/, async () => {
  await authenticationPage.navigateTo();
  await authenticationPage.waitForPresenceOfPage();
});

When(/^I sign in with valid credentials$/, async () => {
  await authenticationPage.fillInUsernameField("test");
  await authenticationPage.fillInPasswordField("test123");
  await authenticationPage.clickSignIn();
});

When(/^I sign in with invalid credentials$/, async () => {
  await authenticationPage.fillInUsernameField("wronguser");
  await authenticationPage.fillInPasswordField("wrongpassword");
  await authenticationPage.clickSignIn();
});

Then(/^The secure page should be displayed$/, async () => {
  await securePage.waitForPresenceOfPage();
  await expect(securePage.isPresent()).to.eventually.equal(true);
});

Then(/^I should see a message that my credentials are invalid$/, async () => {
  await authenticationPage.waitForPresenceOf(authenticationPage.errorMessage);
  await expect(authenticationPage.errorMessage.getText()).to.eventually.equal(
    "Wrong username or password. Please retry."
  );
});
```

Notice that we acces multiple pageobject functions.

#### Support

Here we define our hooks:

```typescript
import { browser } from "protractor";
import { config } from "../protractor-jenkins.conf";

const { BeforeAll, After, Status } = require("cucumber");

BeforeAll({ timeout: 15 * 1000 }, async () => {
  await browser.get(config.baseUrl);
});

After(async function(scenario) {
  await browser.executeScript("window.localStorage.clear();");
  await browser.executeScript("window.sessionStorage.clear();");

  if (scenario.result.status === Status.FAILED) {
    await browser
      .manage()
      .logs()
      .get("browser")
      .then(function(browserLog) {
        console.log("\n\r############# START CONSOLE OUTPUT ##############");
        console.log("log: " + require("util").inspect(browserLog));
        console.log("############## END CONSOLE OUTPUT ###############\n\r");
      });
    const screenShot = await browser.takeScreenshot();
    this.attach(screenShot, "image/png");
  }
});
```

#### Reporter

```typescript
import * as reporter from "cucumber-html-reporter";
import * as fs from "fs";
import * as mkdirp from "mkdirp";
import * as path from "path";
const jsonReports = path.join(process.cwd(), "/integration/reports/json");
const htmlReports = path.join(process.cwd(), "/integration/reports/html");
const targetJson = jsonReports + "/cucumber_report.json";
const targetHtml = htmlReports + "/cucumber_report.html";

const cucumberReporterOptions = {
  jsonFile: targetJson,
  output: targetHtml,
  reportSuiteAsScenarios: true,
  theme: "bootstrap"
};

export class Reporter {
  public static createDirectory(dir: string) {
    if (!fs.existsSync(dir)) {
      mkdirp.sync(dir);
    }
  }

  public static createHTMLReport() {
    try {
      reporter.generate(cucumberReporterOptions); // invoke cucumber-html-reporter
    } catch (err) {
      if (err) {
        throw new Error("Failed to save cucumber test results to json file.");
      }
    }
  }
}
```

#### Testdata

Mock data inside `.json` files, used as a response as if it was a real backend.
