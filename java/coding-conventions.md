---
title: "Java Coding Conventions"
menu:
  foundation:
    title: "Java Coding conventions"
    parent: "Java"
weight: 1
---

## Google Java Style Guide

Our Java coding conventions are based on the [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html). Exceptions or additions to the Google Java Style Guide can later be defined here.


## Formatter for IntelliJ IDEA

Google provides formatters for [IntelliJ IDEA](https://raw.githubusercontent.com/google/styleguide/gh-pages/intellij-java-google-style.xml) and [Eclipse](https://raw.githubusercontent.com/google/styleguide/gh-pages/eclipse-java-google-style.xml).

## Additions to the Google Java Style Guide

### Minimize mutability (Effective Java item 17)

Immutable classes are safer to use and easier to reason about. 
This makes them less prone to error and more secure. 
As a rule of thumb, make all fields final, unless you have a very good reason to make them mutable.
Also make the class final so it cannot be extended by a class that adds non-final fields.

### Design for extension, otherwise prohibit it (Effective Java item 19)

All classes should be made final, unless they are consciously designed to be extended.
When a class is not final, document how it must be extended.
Also, make all methods final that are not designed to be overridden. 