---
title: "Common Java Libraries"
menu:
  foundation:
    title: "Common Java Libraries"
    parent: "Java"
weight: 2
---

This section provides a set of commonly used Java libraries. Functionality between libraries often overlaps. Libraries in this section are sorted in order of preference. For example: prefer Guava over Apache Commons Lang. 

## General purpose

### Guava

[Guava](https://github.com/google/guava) is a set of core libraries that includes new collection types (such as multimap and multiset), immutable collections, a graph library, functional types, an in-memory cache, and APIs/utilities for concurrency, I/O, hashing, primitives, reflection, string processing, and much more!

### Apache Commons Lang

[Lang](https://commons.apache.org/proper/commons-lang/) provides a host of helper utilities for the java.lang API, notably String manipulation methods, basic numerical methods, object reflection, concurrency, creation and serialization and System properties. Additionally it contains basic enhancements to java.util.Date and a series of utilities dedicated to help with building methods, such as hashCode, toString and equals.

## Testing

### JUnit 5

[JUnit 5](https://junit.org/junit5/) is a unit testing library that provides an extensive set of assertions.
If you don't need mocks, JUnit is probably all you need.

### Mockito

[Mockito](https://site.mockito.org/) is a mocking library for Java.
It can be used in conjunction with JUnit.
Mockito can emulate objects that are not directly under test.
This makes it easier to make sure your code does the right thing and that a failure is not due to problem in another class.

### AssertJ

[AssertJ](http://joel-costigliola.github.io/assertj/) provides fluent assertions and can be combined with JUnit.
The advantage of fluent assertions is that they read more naturally.

## Database

### Java Persistence API (JPA)

[Java Persistence API](https://en.wikipedia.org/wiki/Java_Persistence_API) is a standard Java API for database access.
It provides annotations to map objects to relational tables and an API to access the data.

### Hibernate

[Hibernate](https://hibernate.org/) is an object-relational mapper that implements the JPA specification.
While you can use Hibernate on its own, it is best used in conjunction with JPA.

### Flyway

[Flyway](https://flywaydb.org/) is a database schema migration tool that can be built into your application.

## Application frameworks

### Spring Boot

[Spring Boot](https://spring.io/projects/spring-boot) is a very complete framework for writing web applications. 
Spring Boot leverages the Spring Framework and makes it easy to access the database, configure your application, write web services and solves lots of other common problems in business applications.

## HTTP clients

### Retrofit

[Retrofit](https://square.github.io/retrofit/) makes it easy to talk to an external HTTP service.
All it needs is an annotated interface to provide a fully functional service client.

### Apache HTTP Client

[Apache HTTP Client](https://hc.apache.org/httpcomponents-client-ga/index.html) provides a lower level HTTP client, which gives you more fine grained control.

## Data import / export

### Mustache

[Mustache](http://mustache.github.io/) is a templating library that can be used to generate HTML, XML, or any text based format.

### Open HTML to PDF

[Open HTML to PDF](https://github.com/danfickle/openhtmltopdf) generates PDF documents from HTML 5 and CSS 2.1.
Combined with Mustache, generating PDF documents becomes easy for anyone with HTML / CSS skills.

### Apache POI

[Apache POI](https://poi.apache.org/) is a library for generating and reading Microsoft Documents like Excel sheets and Word documents.
