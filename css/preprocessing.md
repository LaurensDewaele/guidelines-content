---
title: Preprocessing
menu:
  foundation:
    title: "Preprocessing"
    parent: "CSS"
weight: 1
---

We use [SASS](http://sass-lang.com/) as a preprocessor with the **SCSS syntax** to write our css.

Sass (Syntactically Awesome Style Sheets) is an extension of CSS that enables you to use things like variables, nested rules, inline imports and more. It also helps to keep things organised and allows you to create style sheets faster.

For more info, please read [the SASS guide](http://sass-lang.com/guide).
