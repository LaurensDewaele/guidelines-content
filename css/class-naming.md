---
title: Class Naming
menu:
  foundation:
    title: "Class Naming"
    parent: "CSS"
weight: 4
---

We use a BEM-like syntax with some custom accents.
We only use classes for styling, with the following ingredients:

```scss
.m-component                      // Component
.m-component__element             // Child
.m-component__element__element    // Grandchild

.-modifier                        // Single property modifier, can be chained

.m-component--variation           // Standalone variation of a component
.m-component__element--variation  // Standalone variation of a child

.js-hook                          // Javascript hook, not used for styling
```

## .component and .component__element

```html
<header class="m-hero">
```

- A single reusable component or pattern
- Children are separated with double underscores `__`
- All lowercase, can contain `-` in name
- Avoid more than 3 levels deep

```html
<header class="m-hero">
  <h2 class="m-hero__title">
    <small class="m-hero__title__extra">
```

Be descriptive with component elements. Consider `class="team__member"` instead of `class="team__item"`

```html
<ul class="m-team">
  <li class="m-team__member">
```

## .-modifier

```html
<button type="button" class="a-btn -disabled">
```

```scss
.a-btn {

  &.-disabled {
    opacity: .5;
    cursor: not-allowed;
  }
}
```

- A modifier changes only simple properties of a component, or adds a property
- Modifiers are **always tied** to a component, don't work on their own (make sure you never write "global" modifier selectors)
- Multiple modifiers are possible. Each modifier is responsible for a property: `class="alert -success -rounded -large"`. If you keep using these modifiers together, consider a **variation** (see below)
- Since modifiers have a single responsability, the order in HTML or CSS shouldn't matter

## .component--variation

```html
<button type="button" class="a-btn--delete">
```

```scss
.a-btn--delete {
  // Extend base
  @extend "a-btn";

  // Added Variation
  color: $white;
  text-transform: uppercase;

  background-color: $red;
}
```

- A variation adds more than one property at once to a class, and acts as a shorthand for multiple modifiers often used together.
- It is used stand-alone without the need to use the base class `button`
- Even variations should be generic and reusable if possible: `class="team--large"` is better than `class="team--management"`

## .js-hook

```html
<figure class="js-map …" data-map-icon="url.png" data-map-lat="4.56" data-map-lon="1.23"></figure>
```

- Use `js-hook` to initiate handlers like `document.getElementsByClassName('js-hook')`
- Use `data-attributes` only for data storage or configuration storage
- Has no effect on styling whatsoever (never ever hook styling on a .js- class)

## Class order

- Always put the javascript hooks first
- Followed by the component classes
- Place the modifiers last

```html
<li class="js-hook m-team__member -new">
```