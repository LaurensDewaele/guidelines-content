---
title: File Organization
menu:
  foundation:
    title: "File Organization"
    parent: "CSS"
weight: 2
---

This is our base folder structure for our styling. we typically have 7 folders and a main `app.scss` file.

```markdown
├ settings    : Setting trough variables
├ tools       : Mixins and functions
├ base        : Normalize and box-sizing
├ typography  : Basic typography
├ animations  : Shared @keyframes declarations
├ components  : Atoms, Molecules, Organisms, ...
├ utilities   : Small utility classes
└ app.scss    : Main file with includes
```

## Settings folder

Variables for colors, breakpoints, general spacings, font-sizes, etc.
In the settings folder, we typically use 3 files and a main `_settings.scss` file.

> It’s important not to output any CSS in this layer.

```markdown
├ _colors.scss       : Variables for your colors
├ _scaffolding.scss  : Variables for your scaffolding
├ _typography.scss   : Variables for your typography
└ _settings.scss     : Main file with includes
```

## Tools folder

Globally used mixins and functions.
In the tools folder, we typically use 2 folders and a main `_tools.scss` file.

> It’s important not to output any CSS in this layer.

```markdown
├ _functions.scss   : Globally used functions
├ _mixins.scss      : Globally used mixins
└ _tools.scss       : Main file with includes
```

## Base folder

In the base folder, we define our normalize, setup our scaffolding and set our grid.

We typically use 3 files and a main `_base.scss` file.

```markdown
├ _normalize.scss   : Normalize for your project
├ _scaffolding.scss : Base styling for box-sizing, ...
├ _grid.scss        : Base styling for the container, grid, ...
└ _base.scss        : Main file with includes
```

## Typography folder

In the typography folder, we set our default typographic styles.

```markdown
├ _headers.scss     : Base styling for h1, h2, h3, ...
├ _bodycopy.scss    : Base styling for p, ...
├ ...
└ _typograhpy.scss  : Main file with includes
```

## Animations folder

In the animations folder, we set our shared @keyframe animations.

```markdown
├ _fadeIn.scss      : @keyframes for fadeIn animation
├ _fadeOut.scss     : @keyframes for fadeOut animation
├ ...
└ _animations.scss  : Main file with includes
```

## Components folder

The components folder is the center of our styling endevour.
Based on our **design system**, we use the [Atomic Design Methodology](http://atomicdesign.bradfrost.com/chapter-2/) to define it's structure.

### Atomic Design

Atomic design is a methodology composed of five distinct stages working together to create interface design systems in a more deliberate and hierarchical manner.

Must read: [the Atomic Design guide](http://atomicdesign.bradfrost.com/chapter-2/).

The **five stages** of atomic design are:

#### Atoms

The foundational building blocks of our application.

#### Molecules

Relatively simple groups of UI elements functioning together as a unit.

#### Organisms

Organisms are relatively complex UI components composed of groups of molecules and/or atoms and/or other organisms.

#### Templates

Templates are page-level objects that place components into a layout and articulate the design’s underlying content structure.

#### Pages

Pages are specific instances of templates that show what a UI looks like with real representative content in place.

### Folder structure

This folder includes stand-alone reusable components with their modifiers and variations.
Based on the Atomic CSS principals, we typically use 6 folders and a main `_components.scss` file:

```markdown
├ 01_atoms          : Folder for your Atoms
├ 02_molecules      : Folder for your Molecules
├ 03_organisms      : Folder for your Organisms
├ 04_templates      : Folder for your Templates
├ 05_pages          : Folder for your Pages
├ 06_vendors        : Folder for your Vendors
└ _components.scss  : Main file with includes
```

### Atoms folder

Atoms are the foundational building blocks of our application.

Atom classnames are prefixed with an `a-` to imply it's identity.

```markdown
01_atoms/
└  _a-button.scss
```

### Molecules folder

Molecules are relatively simple groups of UI elements functioning together as a unit.

Molecule classnames are prefixed with an `m-` to imply it's identity.

```markdown
02_molecules/
├  _m-teaser.scss
├  _m-menu.scss
└  _m-card.scss
```

### Organisms folder

Organisms are relatively complex UI components composed of groups of molecules and/or atoms and/or other organisms.

Organism classnames are prefixed with an `o-` to imply it's identity.

```markdown
03_organisms/
├  _o-teaser-list.scss
└  _o-card-list.scss
```

### Templates folder

Templates are page-level objects that place components into a layout and articulate the design’s underlying content structure.

Template classnames are prefixed with an `t-` to imply it's identity.

```markdown
04_templates/
├  _t-section.scss
└  _t-article.scss
```

### Pages folder

Pages are specific instances of templates that show what a UI looks like with real representative content in place.

Page classnames are prefixed with an `p-` to imply it's identity.

```markdown
05_pages/
├  _p-blog.scss
└  _p-home.scss
```

### Vendors folder

Styles necessary for your vendors get a home here.

```markdown
├ _select-2.scss
├ _flickety.scss
└ ...
```