---
title: Best Practices
menu:
  foundation:
    title: "Best Practices"
    parent: "CSS"
weight: 5
---



## Never use a fixed value for the root font-size

Use percentage values on the root element for font-size to avoid breaking the accessibility features of your browser.

```scss
html {
  font-size: 100%; // Results in 16px in most browser
}
```

## Use `rem` and `em`

Use rem's for margins and fixed spacings, em's for padding and relative sizing. This is not a strict rule and can be adjusted depending on the situation.

```scss
.o-page-header {
  margin: 1rem 0;
  padding: 1em 0;
}
```

## Limit shorthand notation

Strive to limit use of shorthand declarations to instances where you must explicitly set all the available values. Common overused shorthand properties include:

- padding
- margin
- font
- background
- border
- border-radius
- transition
- ...

Often we don't need to set all the values a shorthand property represents. For example, HTML headings only set top and bottom margin, so when necessary, only override those two values. Excessive use of shorthand properties often leads to sloppier code with unnecessary overrides and unintended side effects.

```scss
.m-page-header {
  // Bad example =(
  margin: 0 0 1rem;

  border-radius: .3em .3em 0 0;

  transition: background .3s ease;


  // Good example =)
  margin-bottom: 1rem;

  border-top-left-radius: .3em;
  border-top-right-radius: .3em;

  transition: background-color .3s ease;
}
```