---
title: Dom Structure
menu:
  foundation:
    title: "Dom Structure"
    parent: "CSS"
weight: 3
---


- All styling is done by classes (except for HTML that is out of our control). This for optimum rendering performance and maintainability.
- Avoid #id's for styling
- Make elements easily reusable and moveable in a project or between projects
- Avoid multiple components on 1 DOM-element. Break them up.

```html
<!-- Try to avoid, article padding or margin could break the grid-->
<article class="container t-article">
  …
</article>

<!-- More flexible, readable & moveable -->
<main role="main" class="container">
  <article class="t-article">
    …
  </article>
</main>
```

Tags are interchangeable since styling is done by class.

```html
<!-- All the same -->
<div class="t-article">
<main class="t-article">
<section class="t-article">
<article class="t-article">
```

Html tags that are out of control (eg. the output of an editor) are scoped by the component.

```html
<article class="t-article">
  <!-- custom html output -->
</article>
```

```scss
.t-article {
  // Tag instead of class here
  & h2 {
    …
  }

  & p {
    …
  }
}
```