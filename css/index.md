---
title: CSS
menu: foundation
weight: 3
---

Writing **CSS** for **large scale projects** isn't your average kind of CSS writing.
We need a set of guidlines to prevent it from getting out of control.

> Replace "can you build this?" with "can you maintain this without losing your mind?" -Nicolas Gallagher

## Preprocessing

We use [SASS](http://sass-lang.com/) as a preprocessor with the **SCSS syntax** to write our css.

Sass (Syntactically Awesome Style Sheets) is an extension of CSS that enables you to use things like variables, nested rules, inline imports and more. It also helps to keep things organised and allows you to create style sheets faster.

For more info, please read [the SASS guide](http://sass-lang.com/guide).

## File Organization

This is our base folder structure for our styling. we typically have 7 folders and a main `app.scss` file.

```markdown
├ settings    : Setting trough variables
├ tools       : Mixins and functions
├ base        : Normalize and box-sizing
├ typography  : Basic typography
├ animations  : Shared @keyframes declarations
├ components  : Atoms, Molecules, Organisms, ...
├ utilities   : Small utility classes
└ app.scss    : Main file with includes
```

### Settings folder

Variables for colors, breakpoints, general spacings, font-sizes, etc.
In the settings folder, we typically use 3 files and a main `_settings.scss` file.

> It’s important not to output any CSS in this layer.

```markdown
├ _colors.scss       : Variables for your colors
├ _scaffolding.scss  : Variables for your scaffolding
├ _typography.scss   : Variables for your typography
└ _settings.scss     : Main file with includes
```

### Tools folder

Globally used mixins and functions.
In the tools folder, we typically use 2 folders and a main `_tools.scss` file.

> It’s important not to output any CSS in this layer.

```markdown
├ _functions.scss   : Globally used functions
├ _mixins.scss      : Globally used mixins
└ _tools.scss       : Main file with includes
```

### Base folder

In the base folder, we define our normalize, setup our scaffolding and set our grid.

We typically use 3 files and a main `_base.scss` file.

```markdown
├ _normalize.scss   : Normalize for your project
├ _scaffolding.scss : Base styling for box-sizing, ...
├ _grid.scss        : Base styling for the container, grid, ...
└ _base.scss        : Main file with includes
```

### Typography folder

In the typography folder, we set our default typographic styles.

```markdown
├ _headers.scss     : Base styling for h1, h2, h3, ...
├ _bodycopy.scss    : Base styling for p, ...
├ ...
└ _typograhpy.scss  : Main file with includes
```

### Animations folder

In the animations folder, we set our shared @keyframe animations.

```markdown
├ _fadeIn.scss      : @keyframes for fadeIn animation
├ _fadeOut.scss     : @keyframes for fadeOut animation
├ ...
└ _animations.scss  : Main file with includes
```

### Components folder

The components folder is the center of our styling endevour.
Based on our **design system**, we use the [Atomic Design Methodology](http://atomicdesign.bradfrost.com/chapter-2/) to define it's structure.

#### Atomic Design

Atomic design is a methodology composed of five distinct stages working together to create interface design systems in a more deliberate and hierarchical manner.

Must read: [the Atomic Design guide](http://atomicdesign.bradfrost.com/chapter-2/).

The **five stages** of atomic design are:

##### Atoms

The foundational building blocks of our application.

##### Molecules

Relatively simple groups of UI elements functioning together as a unit.

##### Organisms

Organisms are relatively complex UI components composed of groups of molecules and/or atoms and/or other organisms.

##### Templates

Templates are page-level objects that place components into a layout and articulate the design’s underlying content structure.

##### Pages

Pages are specific instances of templates that show what a UI looks like with real representative content in place.

#### Folder structure

This folder includes stand-alone reusable components with their modifiers and variations.
Based on the Atomic CSS principals, we typically use 6 folders and a main `_components.scss` file:

```markdown
├ 01_atoms          : Folder for your Atoms
├ 02_molecules      : Folder for your Molecules
├ 03_organisms      : Folder for your Organisms
├ 04_templates      : Folder for your Templates
├ 05_pages          : Folder for your Pages
├ 06_vendors        : Folder for your Vendors
└ _components.scss  : Main file with includes
```

#### Atoms folder

Atoms are the foundational building blocks of our application.

Atom classnames are prefixed with an `a-` to imply it's identity.

```markdown
01_atoms/
└  _a-button.scss
```

#### Molecules folder

Molecules are relatively simple groups of UI elements functioning together as a unit.

Molecule classnames are prefixed with an `m-` to imply it's identity.

```markdown
02_molecules/
├  _m-teaser.scss
├  _m-menu.scss
└  _m-card.scss
```

#### Organisms folder

Organisms are relatively complex UI components composed of groups of molecules and/or atoms and/or other organisms.

Organism classnames are prefixed with an `o-` to imply it's identity.

```markdown
03_organisms/
├  _o-teaser-list.scss
└  _o-card-list.scss
```

#### Templates folder

Templates are page-level objects that place components into a layout and articulate the design’s underlying content structure.

Template classnames are prefixed with an `t-` to imply it's identity.

```markdown
04_templates/
├  _t-section.scss
└  _t-article.scss
```

#### Pages folder

Pages are specific instances of templates that show what a UI looks like with real representative content in place.

Page classnames are prefixed with an `p-` to imply it's identity.

```markdown
05_pages/
├  _p-blog.scss
└  _p-home.scss
```

#### Vendors folder

Styles necessary for your vendors get a home here.

```markdown
├ _select-2.scss
├ _flickety.scss
└ ...
```

## DOM structure

- All styling is done by classes (except for HTML that is out of our control). This for optimum rendering performance and maintainability.
- Avoid #id's for styling
- Make elements easily reusable and moveable in a project or between projects
- Avoid multiple components on 1 DOM-element. Break them up.

```html
<!-- Try to avoid, article padding or margin could break the grid-->
<article class="container t-article">
  …
</article>

<!-- More flexible, readable & moveable -->
<main role="main" class="container">
  <article class="t-article">
    …
  </article>
</main>
```

Tags are interchangeable since styling is done by class.

```html
<!-- All the same -->
<div class="t-article">
<main class="t-article">
<section class="t-article">
<article class="t-article">
```

Html tags that are out of control (eg. the output of an editor) are scoped by the component.

```html
<article class="t-article">
  <!-- custom html output -->
</article>
```

```scss
.t-article {
  // Tag instead of class here
  & h2 {
    …
  }

  & p {
    …
  }
}
```

## Class Naming

We use a BEM-like syntax with some custom accents.
We only use classes for styling, with the following ingredients:

```scss
.m-component                      // Component
.m-component__element             // Child
.m-component__element__element    // Grandchild

.-modifier                        // Single property modifier, can be chained

.m-component--variation           // Standalone variation of a component
.m-component__element--variation  // Standalone variation of a child

.js-hook                          // Javascript hook, not used for styling
```

### .component and .component__element

```html
<header class="m-hero">
```

- A single reusable component or pattern
- Children are separated with double underscores `__`
- All lowercase, can contain `-` in name
- Avoid more than 3 levels deep

```html
<header class="m-hero">
  <h2 class="m-hero__title">
    <small class="m-hero__title__extra">
```

Be descriptive with component elements. Consider `class="team__member"` instead of `class="team__item"`

```html
<ul class="m-team">
  <li class="m-team__member">
```

### .-modifier

```html
<button type="button" class="a-btn -disabled">
```

```scss
.a-btn {

  &.-disabled {
    opacity: .5;
    cursor: not-allowed;
  }
}
```

- A modifier changes only simple properties of a component, or adds a property
- Modifiers are **always tied** to a component, don't work on their own (make sure you never write "global" modifier selectors)
- Multiple modifiers are possible. Each modifier is responsible for a property: `class="alert -success -rounded -large"`. If you keep using these modifiers together, consider a **variation** (see below)
- Since modifiers have a single responsability, the order in HTML or CSS shouldn't matter

### .component--variation

```html
<button type="button" class="a-btn--delete">
```

```scss
.a-btn--delete {
  // Extend base
  @extend "a-btn";

  // Added Variation
  color: $white;
  text-transform: uppercase;

  background-color: $red;
}
```

- A variation adds more than one property at once to a class, and acts as a shorthand for multiple modifiers often used together.
- It is used stand-alone without the need to use the base class `button`
- Even variations should be generic and reusable if possible: `class="team--large"` is better than `class="team--management"`

### .js-hook

```html
<figure class="js-map …" data-map-icon="url.png" data-map-lat="4.56" data-map-lon="1.23"></figure>
```

- Use `js-hook` to initiate handlers like `document.getElementsByClassName('js-hook')`
- Use `data-attributes` only for data storage or configuration storage
- Has no effect on styling whatsoever (never ever hook styling on a .js- class)

### Class order

- Always put the javascript hooks first
- Followed by the component classes
- Place the modifiers last

```html
<li class="js-hook m-team__member -new">
```

## Best Practices

### Never use a fixed value for the root font-size

Use percentage values on the root element for font-size to avoid breaking the accessibility features of your browser.

```scss
html {
  font-size: 100%; // Results in 16px in most browser
}
```

### Use `rem` and `em`

Use rem's for margins and fixed spacings, em's for padding and relative sizing. This is not a strict rule and can be adjusted depending on the situation.

```scss
.o-page-header {
  margin: 1rem 0;
  padding: 1em 0;
}
```

### Limit shorthand notation

Strive to limit use of shorthand declarations to instances where you must explicitly set all the available values. Common overused shorthand properties include:

- padding
- margin
- font
- background
- border
- border-radius
- transition
- ...

Often we don't need to set all the values a shorthand property represents. For example, HTML headings only set top and bottom margin, so when necessary, only override those two values. Excessive use of shorthand properties often leads to sloppier code with unnecessary overrides and unintended side effects.

```scss
.m-page-header {
  // Bad example =(
  margin: 0 0 1rem;

  border-radius: .3em .3em 0 0;

  transition: background .3s ease;


  // Good example =)
  margin-bottom: 1rem;

  border-top-left-radius: .3em;
  border-top-right-radius: .3em;

  transition: background-color .3s ease;
}
```

## Format

### Group style-lines by declaration

```scss
.m-page-header {
  // Positioning
  z-index: 10;
  position: absolute;
  top: 0;
  left: 0;

  // Box-model
  display: block;
  float: left;
  margin: 1rem 0;
  padding: 1em 0;

  // Typography
  font-family: $font-family--headers;
  font-size: 4rem;
  line-height: 1.3;
  color: $sweet-mustard;

  // Visual
  background: $black;
  border: 2px solid $sweet-mustard;
  border-radius: .4em;
  opacity: .5;

  // Misc
  transform: translate3d(0, 2rem, 1rem);
  transition: color .3s ease;
}
```

### No leading zero

Don't prefix property values or color parameters with a leading zero (e.g., .5 instead of 0.5 and -.5rem instead of -0.5rem).

```scss
.m-page-header {
  margin-top: -.5rem;

  opacity: .5;
}
```

### Avoid specifying units for zero values

Avoid specifying units for zero values, e.g., margin: 0; instead of margin: 0px;.

```scss
.m-page-header {
  margin: 1rem 0;
  padding: 1em 0;
}
```

### Use lowercase and six character hex values if possible

Lowercase all hex values, e.g., #ffffff. Lowercase letters are much easier to discern when scanning a document as they tend to have more unique shapes.

```scss
$white: #ffffff;
$black: #111111;
```

### Semicolons

Always use a semicolon as a terminator, not only as a seperator.

```scss
.a-btn {
  position: fixed;
  top: 0;
  right: 0;

  display: grid;
  margin: 1rem 0;

  font-size: 4rem;
  color: $white;
}
```

### Box model

The box model should ideally be the same for the entire document. A global `{ box-sizing: border-box; }` is fine, but don't change the default box model on specific elements if you can avoid it.

### Flow

Don't change the default behavior of an element if you can avoid it. Keep elements in the natural document flow as much as you can, also do not take an element off the flow if you can avoid it.

### Positioning

There are many ways to position elements in CSS but try to restrict yourself to the properties/values below. By order of preference:

```scss
display: block;
display: flex;
position: relative;
position: sticky;
position: absolute;
position: fixed;
```

### Specificity

Don't make values and selectors hard to override. Minimize the use of id's and avoid !important.

### Animations

Favor transitions over animations. Avoid animating other properties than opacity and transform.

```scss
figure {

  &:hover {
    transition: 1s;
    transform: translateX(100px);
  }
}
```

### Drawing

Avoid HTTP requests when the resources are easily replicable with CSS, e.g drawing a circle.

```scss
figure {

  &::before {
    content: "";

    display: block;
    width: 1rem;
    height: 1rem;

    border-radius: 50%;
    background: $white;
  }
}
```

### Nesting

#### Put extends at the top

```scss
.a-btn--delete {
  // Extend base
  @extend "a-btn";

  // Variations
  background-color: $red;
  color: $white;
}
```

#### Spacing

Always use a empty new line above and below nested selectors.

```scss
.a-btn {
  font-size: 1rem;

  &.-disabled {
    opacity: .5;
    cursor: not-allowed;
  }

  &.-large {
    font-size: 2rem;
  }
}
```

#### Pseudo-classes and pseudo-elements

- Put the pseudo-classes and pseudo-elements at the bottom of your component (but above the media-queries and nested classes)
- Pseudo-classes are proceded with a single colon (`:hover`), pseudo-elements with two colons (`::before`).

```scss
.a-btn--delete {

  &:hover, &:focus {
    ...
  }

  &::before, &::after {
    ...
  }
}
```

#### Media-queries

- Put media-queries at the absolute bottom.
- Place them inside your selectors.
- Use variables to define your breakpoints.

```scss
.m-page-header {

  @media (min-width: $breakpoint--md) {
    ...
  }
}
```

#### Full example

```scss
.m-page-header {
  // Extends at the top
  @extend "m-other-header";

  // Styles grouped by declaration
  position: fixed;
  top: 0;
  left: 0;
  right: 0;

  display: grid;
  padding: 0 2em;
  margin: 1rem 0;

  font-size: 4rem;
  color: $white;

  background: $red;

  transition: background-color .2s ease;

  // Modifiers
  &.-small {
    font-size: 2rem;
  }

  // Pseudo-classes and pseudo-elements
  &:hover, &:focus {
    background-color: darken($red, 5%);
  }

  &::before, &::after {
    content: "";
  }

  // Media-queries
  @media (min-width: $breakpoint--md) {
    position: static;
  }
}
```
