---
title: Format Guideline
menu:
  foundation:
    title: "Format Guideline"
    parent: "CSS"
weight: 5
---

## Group style-lines by declaration

```scss
.m-page-header {
  // Positioning
  z-index: 10;
  position: absolute;
  top: 0;
  left: 0;

  // Box-model
  display: block;
  float: left;
  margin: 1rem 0;
  padding: 1em 0;

  // Typography
  font-family: $font-family--headers;
  font-size: 4rem;
  line-height: 1.3;
  color: $sweet-mustard;

  // Visual
  background: $black;
  border: 2px solid $sweet-mustard;
  border-radius: .4em;
  opacity: .5;

  // Misc
  transform: translate3d(0, 2rem, 1rem);
  transition: color .3s ease;
}
```

## No leading zero

Don't prefix property values or color parameters with a leading zero (e.g., .5 instead of 0.5 and -.5rem instead of -0.5rem).

```scss
.m-page-header {
  margin-top: -.5rem;

  opacity: .5;
}
```

## Avoid specifying units for zero values

Avoid specifying units for zero values, e.g., margin: 0; instead of margin: 0px;.

```scss
.m-page-header {
  margin: 1rem 0;
  padding: 1em 0;
}
```

## Use lowercase and six character hex values if possible

Lowercase all hex values, e.g., #ffffff. Lowercase letters are much easier to discern when scanning a document as they tend to have more unique shapes.

```scss
$white: #ffffff;
$black: #111111;
```

## Semicolons

Always use a semicolon as a terminator, not only as a seperator.

```scss
.a-btn {
  position: fixed;
  top: 0;
  right: 0;

  display: grid;
  margin: 1rem 0;

  font-size: 4rem;
  color: $white;
}
```

## Box model

The box model should ideally be the same for the entire document. A global `{ box-sizing: border-box; }` is fine, but don't change the default box model on specific elements if you can avoid it.

## Flow

Don't change the default behavior of an element if you can avoid it. Keep elements in the natural document flow as much as you can, also do not take an element off the flow if you can avoid it.

## Positioning

There are many ways to position elements in CSS but try to restrict yourself to the properties/values below. By order of preference:

```scss
display: block;
display: flex;
position: relative;
position: sticky;
position: absolute;
position: fixed;
```

## Specificity

Don't make values and selectors hard to override. Minimize the use of id's and avoid !important.

## Animations

Favor transitions over animations. Avoid animating other properties than opacity and transform.

```scss
figure {

  &:hover {
    transition: 1s;
    transform: translateX(100px);
  }
}
```

## Drawing

Avoid HTTP requests when the resources are easily replicable with CSS, e.g drawing a circle.

```scss
figure {

  &::before {
    content: "";

    display: block;
    width: 1rem;
    height: 1rem;

    border-radius: 50%;
    background: $white;
  }
}
```

## Nesting

### Put extends at the top

```scss
.a-btn--delete {
  // Extend base
  @extend "a-btn";

  // Variations
  background-color: $red;
  color: $white;
}
```

### Spacing

Always use a empty new line above and below nested selectors.

```scss
.a-btn {
  font-size: 1rem;

  &.-disabled {
    opacity: .5;
    cursor: not-allowed;
  }

  &.-large {
    font-size: 2rem;
  }
}
```

### Pseudo-classes and pseudo-elements

- Put the pseudo-classes and pseudo-elements at the bottom of your component (but above the media-queries and nested classes)
- Pseudo-classes are proceded with a single colon (`:hover`), pseudo-elements with two colons (`::before`).

```scss
.a-btn--delete {

  &:hover, &:focus {
    ...
  }

  &::before, &::after {
    ...
  }
}
```

### Media-queries

- Put media-queries at the absolute bottom.
- Place them inside your selectors.
- Use variables to define your breakpoints.

```scss
.m-page-header {

  @media (min-width: $breakpoint--md) {
    ...
  }
}
```

### Full example

```scss
.m-page-header {
  // Extends at the top
  @extend "m-other-header";

  // Styles grouped by declaration
  position: fixed;
  top: 0;
  left: 0;
  right: 0;

  display: grid;
  padding: 0 2em;
  margin: 1rem 0;

  font-size: 4rem;
  color: $white;

  background: $red;

  transition: background-color .2s ease;

  // Modifiers
  &.-small {
    font-size: 2rem;
  }

  // Pseudo-classes and pseudo-elements
  &:hover, &:focus {
    background-color: darken($red, 5%);
  }

  &::before, &::after {
    content: "";
  }

  // Media-queries
  @media (min-width: $breakpoint--md) {
    position: static;
  }
}
```