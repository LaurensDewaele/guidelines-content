---
title: "C# Coding Conventions"
menu:
  foundation:
    title: ".NET Coding Conventions"
    parent: ".NET"
weight: 2
---

## Introduction

In order to maintain readable, structured code we need a series of standardized conventions and coding style, so that you and others can easily read and understand the code.
At **Sweet Mustard** we seek that every .NET developer uses and follows these conventions and coding style, though there are some exceptions.

## EditorConfig

A lot of the code style rules can be enforced by the use of an `.editorconfig` file added to the solution.

An `.editorconfig` file for .NET projects can be found in our public [editorconfig repository](https://github.com/sweet-mustard/editorconfig/tree/master/dotnet).

### Using an .editorconfig file in Visual Studio

To use this file, you need to install the `EditorConfig Language Service` extension in Visual Studio. While VS2017 should include native support for `.editorconfig` files, it appears not to work without this extension. Also, changes made to the `.editorconfig` file seems to only apply after restarting Visual Studio.

## Naming conventions

### Use PascalCasing for event-, property, namespace-, class- and methodnames

```csharp
namespace SweetMustard.UserManagement
{
    public class Account
    {
        public string Name{ get; set; }

        public void ChangeName(string newName)
        {
            this.Name = newName;
        }
    }
}
```

### Use camelCasing for method parameters and local variables

```csharp
public class SweetMustard
{
    public void CreateSweety(string newName, string position)
    {
        var newSweety = new Sweety(newName, position);
    }
}
```

### Use predefined type aliases instead of system type names

```csharp
// Good
string firstName;
int lastIndex;
bool isDeleted;

// Bad
String firstName;
Int32 lastIndex;
Boolean isDeleted;
```

### Interface names always start with capital letter I

The name should be a noun or adjective.

```csharp
public interface IGadgetRepository
{
}

public interface IGadgetService
{
}

public interface IGroupable
{
}
```

### Avoid using abbreviations.

 Exceptions do exist such as `Id`, `Ftp`, `Uri`, `Dto` ...

```csharp
// Good
UserGroup userGroup;
Assignment sweetMustardAssignment;

// Bad
UserGroup userGrp;
Assignment SweMuAsgnmnt;

// Exceptions
CustomerId customerId;
XmlDocument xmlDocument;
UriPart uriPart;
```
### DO NOT use include parent class name within a property name.

```csharp
//Good
Customer.Name

//Bad
Customer.CustomerName
```

### DO NOT use Hungarian notation for any other type identification in identifiers

```csharp
// Good
int counter;
string name;

// Bad
int iCounter;
string strName;
```

### DO NOT use all-CAPS for constant or static readonly fields

```csharp
// Good
public const string DeveloperType = "BackEnd";
public static readonly string DeveloperLevel = "Intermediate";

// Bad 
public const string DEVELOPERTYPE = "BackEnd";
public static readonly string DEVELOPER_LEVEL = "Intermediate";
```

### DO NOT use underscores in identifiers.

 Exception: private (readonly) variables.

```csharp
// Good
public DateTime clientAppointment;
public TimeSpan timeLeft;

// Bad
public DateTime client_AppointMent;
public TimeSpan time_left;

// Exception: private (readonly) variables must start with an underscore
private DateTime _registrationDate;
private readonly TimeSpan _cookieLifetime = TimeSpan.FromDays(7);
```
### DO NOT omit acces modifiers

Declare all identifiers with the appropriate acces modifier instead of allowing the default
``` csharp
//Bad
void MethodName(string parameter);

//Good
private void MethodName(string parameter);
public void MethodName(string parameter);
```

### DO NOT suffix enum names with Enum

```csharp
// Good
public enum MustardBrand
{
    DevosLemmens,
    Tierenteyn,
    Wostyn,
    Bister
}

// Bad
public enum MustardBrandEnum
{
    DevosLemmens,
    Tierenteyn,
    Wostyn,
    Bister
}
```

### ViewModels

Each domain object/entity should have its own ViewModel. Mandatory, we use the object's name + `ViewModel` suffix.

```csharp
// EntityName + ViewModel
AccountViewModel
UserViewModel
LocationViewModel

// For CRUD pages include functionality prefix
CreateAccountViewModel
EditAccountViewModel

// NOTE: Create/EditAccountViewModel can inherit from AccountViewModel, as base properties are the same.

// OverviewViewModel that holds several ViewModels
// Example: statistics page that has several charts
StatisticsOverviewViewModel
{
    SalesReportViewModel
    OpportunityReportViewModel
    DebtorReportViewModel
}

// ViewModel that is not related to a single Entity, but binds specific data to a certain page
OrderPageViewModel
MainPageViewModel
```

### Data Transfer Objects

If you use DTO's in your application, we recommend that you should have one DTO class for each entity/domain object, where the name can be suffixed with `Dto`.

```csharp
// <EntityName> + <Dto>
AccountDto
LocationDto
```

### Async Methods

Suffix Async methods with `Async`.

```csharp
//Regular
User GetUserById(int userId);
IEnumerable<User> GetAllUsers();

//Async
Task<User> GetUserByIdAsync(int userId);
Task<IEnumerable<User>> GetAllUsersAsync();
```

## Coding Style

### Tabs vs Spaces

We use the default indentation of 4 spaces, no tabs.

### Declare all member variables at the top of a class, with static/const members at the very top

In general, we follow this order:

* `private const`
* `private static readonly`
* `private readonly`
* `private`
* `public properties`

```csharp
public class Developer
{
    private const string Prefix = "SWEMU";
    private static readonly string Seed = Cryptography.GenerateSeed(32);

    private readonly string _language;
    private int _age;

    public string Number { get; set; }
    public decimal Wage { get; set; }

    // Constructor
    public Developer()
    {
        // ...
    }
}
```

### Curly brackets below method/function/class definition

```csharp
// Good
public void SweetMethod()
{
    // ...
}

// Try no to
public int CalculateHours() {
    // ...
}

// Exception: automated properties
public int Id { get; set; }

// Exception: if-statements with small one-line body
if (bookingService == null)
    throw new ArgumentNullException(nameof(bookingService));
```

### Order of namespaces

Order alphabetically but keep `System` directives on top.

### Summarize methods

Only summarize public methods that are exposed by the project.

This also includes ASP.NET Controller methods.

```csharp
/// <summary>
/// Description of what the method does.
/// </summary>
/// <param name="userId">An optional user id.</param>
/// <returns>Does the method return a view? A result value?</returns>
public ActionResult Index(int userId = null)
{
    // ...
}
```

Make sure to keep summaries up-to-date!

### Commenting

Writing and maintaining comments is expensive and it's the first thing that gets outdated during refactoring or bugfixes.

> If your feel your code is too complex to understand without comments, your code is probably just bad -- Sammy Larbi

Also read [coding without comments](https://blog.codinghorror.com/coding-without-comments/).

### Spacing between functions, methods and field declarations

Try to keep your code files structured. In short: have the amount of whitespace that is easy on the eye. **Think paragraph-like**.

```csharp
// Good example:
public void Method(int number)
{
    var variable = new Variable();
    variable.Property += number;

    if (statement)
    {
        variable.Boolean = true;
        return variable;
    }

    return variable;
}

public void AnotherMethod()
{
    // Do Stuff
}
```

```csharp
// Try not to:
public void Method(int number)
{
    var variable = new Variable();
    variable.Property += number;
    if(statement)
        return variable;
    return variable;
}
public void AnotherMethod()
{
    // Do Stuff
}
```

### Usage of regions

When having bigger CS files, it's advised to use regions. Encapsulate related methods and functions in their own region. Using regions can help organise and structure your code files in a effecient manner.
These regions can be collapsed and expanded.

```csharp
// Model class expanded regions
public class User
{
    #Region Properties

    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }

    #Endregion

    #Region Constructors

    public User()
    {
    }

    public User(string firstName, string lastName)
    {
        this.FirstName = firstName;
        this.LastName = lastName;
    }

    #Endregion
}

// Model class collapsed regions
public class User
{
   + #Region Properties

   + #Region Constructors
}
```

###  Working with lots of arguments

When using Dependency Injection, a lot of dependencies can be injected in the constructor of a class. We try to follow this convention:

> When using more than 3 arguments, sort them under each other instead of creating a long unwrapped list.

```csharp
// Do this:
public AccountController(
    IUnitOfWork unitOfWork,
    IObjectService objectService,
    ILogger logger,
    IHelper helper)
{
    _unitOfWork = unitOfWork;
    // ...
}

// Instead of:
public AccountController(IUnitOfWork unitOfWork, IObjectService objectService, ILogger logger, IHelper helper)
{
    _unitOfWork = unitOfWork;
    // ...
}
```

* The same convention applies for creating objects through a parametric constructor with a lot of arguments.


### ValueTuples

The use of C# 7 `ValueTuple` is only allowed in private methods. Any public method should not take or return a `ValueTuple`.

If a public method needs to take or return a combination of multiple fields, create a result or context class with the required fields.

The problem with tuples is that you can easily swap values without the compiler (or you) noticing. Consider the examples below:

```csharp
// Bad: tuple elements get mistakenly swapped in calling code
public (string FirstName, string LastName) GetPersonNameInfo()
{
    return (FirstName: "Bob", LastName: "Dylan");
}

(string LastName, string FirstName) result = GetPersonNameInfo();

// Bad: tuple elements get mistakenly swapped in method itself
public (string Firstname, string LastName) GetPersonNameInfo()
{
    return ("Dylan", "Bob");
}

// Good
public PersonNameInfo GetPersonNameInfo()
{
    return new PersonNameInfo
    {
        FirstName = "Bob",
        Lastname = "Dylan"
    };
}
```
### Usage of the ? operator

You can express calculations that otherwise need an `if-else` construction more concisely by using the conditional operator.

```csharp

bool IsValid;

// If-Else construction
if (isValid)
{
    expression;
}
else
{
    second_expression;
}

//Conditional expression
IsValid ? expression : second_expression;
```

