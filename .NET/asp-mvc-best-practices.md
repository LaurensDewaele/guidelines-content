---
title: "ASP.NET MVC Best Practices"
menu:
  foundation:
    title: "ASP.NET MVC Best Practices"
    parent: ".NET"
weight: 4
---

In this article we discuss following modules:

* [General best practices](#General-Best-Practices)
* [Security](#Security)
* [Validation](#Validation)
* [Routing](#Routing)
* [Globalization](#Globalization)
* [Razor Syntax](#Razor-Syntax)
* [Javascript/jQuery](#Javascript-Jquery-In-ASP)


## <a id="General-Best-Practices">General Best Practices</a>

### Use only one specific view engine

View engines are used to create HTML markup from your view, a combination of programming code and HTML. ASP has its own engine while the MVC template has a Razor engine. You should only use the specified engine for increased application performance.

ASP.NET supports many engines, but we aim to always use Razor.

```csharp
protected void Application_Start()
{
    ViewEngines.Engines.Clear(); // Clear all engines
    ViewEngines.Engines.Add(new RazorViewEngine());
}
```

### Each view has its own ViewModel

If there is a `View`, it should have a `ViewModel`. It should be used only for databinding and may not contain any presenter code.

Name your `ViewModel` after the model they represent + suffix `ViewModel`, though there are some exceptions. See our C# Coding Conventions topic.


### Usage of ViewBag

In general, we aim to use `ViewModels` as much as possible to transfer data to the `View` and avoid the usage of `ViewBag`.

Depending on the `ViewBag` causes all kind of runtime errors because no type checking happens at compile time.

Some developers use the ViewBag to send a list of dropdown items to the view (for example: a dropdown list for gender selection, cities, ...). It is best practice to add a `List<SelectListItem>` property in your `ViewModel` instead.

There are a few valid cases where it is conventional to use `ViewBag`. For example, the `ViewBag.Title` (for the page title) as presented in the MVC scaffolded template, but we mandadatory try to use the `ViewModel`.

### Partial views

If a part of the UI is displayed on multiple views, it is mandatory to create a partial.
For example: a navigation bar on a certain area of the application that is used on multiple pages.

* **Naming convention**: `Partial views` should always start with a underscore and suffix with 'partial':
    *  __sidebarPartial.cshtml_
    * __loginFormPartial.cshtml_

### Areas

In bigger applications, using a lot of `models`, `views` and `controllers`, it is advised to use `Areas`. Using the standard MVC structure for bigger projects it can be hard to maintain manageability.

Example folder structure of the UI layer of an _ERP_ application using `Areas`.

```markdown
....
├ PresentationLayer (Web.UI Project)
| ├ Areas/
| |  ├ CRM
| |  | ├ Controllers
| |  | └ Views
| |  ├ HR
| |  | ├ Controllers
| |  | └ Views
| |  └ Finance&Administration
| |    ├ Controllers
| |    |   ├ BillingController
| |    |   └ DebtorController
| |    └ Views
| |        ├ BillingOverview.cshtml
| |        └ DebtorOverview.chtml
| ├ Scripts/
| | ├ JavaScript.js
| | └ jQuery.min.js
| ├ Content/
| | ├ Site.css
| | └ Bootstrap.css
└ ...
```

 * In some cases, each `Area` could have "sub`Areas`", again with their correspondending controllers and views. 


## <a id="Security">Security</a>

### AntiForgeryToken

CSRF or XSRF (Cross-Site Request Forgery) attacks is one of the most common security vulnerabilities on any website. ASP.NET has built-in functionality to protect against this type of attack by using the ÀntiForgeryToken`.

### Using AntiForgeryToken in a form

In a View.cshtml file with a form, add the `AntiForgeryToken` (within the form):

```
@using(Html.BeginForm("MethodName, "ControllerName", FormMethod.Post))
{
    @Html.AntiForgeryToken();
    // ...
}
```

Decorate the corresponding POST action method with the `[ValidateAntiForgeryToken]` attribute.

```csharp
[HttpPost]
[ValidateAntiForgeryToken]
public ActionMethod CreateUser(CreateUserViewModel form)
{
    // ...
}
```

### Using AntiForgeryToken in AJAX requests

It is mandatory to use the token on every non-GET action (so PUT,POST,DELETE,PATCH, etc). This also applies to API requests (f.e. made with jQuery/AJAX).

Manually passing the token in an AJAX request goes like this:

``` javascript
$.ajax({
    type: "post",
    dataType: "html",
    url: '@Url.Action("MethodName","ControllerName")'
    data: {
        __RequestVerificationToken: getToken(),
        id: id
    },
    success: function (response) {
        // ...
    }
});
```

Where `getToken` is implemented like this:

```javascript
function getToken() {
    var token = '@Html.AntiForgeryToken()';
    token = $(token).val();
    return token;
}
```

**Useful links**

[A article](http://www.devcurry.com/2013/01/what-is-antiforgerytoken-and-why-do-i.html) on CSRF and AntiForgeryToken in ASP MVC

## <a id="Validation">Validation</a>

### Use data annotations for server-side validation

Use the `System.ComponentModel.DataAnnotations` namespace to access annotations for server side validation. 

**Note:** server-side validation is mandatory.

In our example we demonstrate server side vaidation through `DataAnnotations` when creating a new user.

```csharp
public class CreateUserViewModel
{
    [Required(ErrorMessage="Username is mandatory")] 
    [MaxLength(100)]
    public string Name { get; set; }
}
```

### Modelstate

In the controller, we can check if the `modelstate` is valid. `ModelState.IsValid` determines wether the submitted values statisfy all `DataAnnotation` validation attributes applied to the model properties.

``` csharp
[HttpPost]
public ActionResult CreateUser(CreateUserViewModel createUserForm)
{
    if(ModelState.IsValid)
    {
        //Code to create user
    }

    //Code to handle invalid form
}
```

### Validation feedback to client

**Full validation summary** 

See below method to use to show all validation in one go by using `ValidationSummary`. This `HTML helper method` is placed in the form on the view.

```
<%= Html.ValidationSummary() %>
// or...
@Html.ValidatonSummary()
```

**`ValidationMessageFor()` Method**

Specify a validation message for a single model property

``` html
@model User  
    
@Html.EditorFor(m => m.Name) <br />
@Html.ValidationMessageFor(m => m.Name, "", new { @class = "text-danger" })
```

In the above example, the first parameter in `ValidationMessageFor` method is a lambda expression to specify a property for which we want to show the error message. You can also add a custom message, if not defined in the `attribute` on the `property`.

* The `ValidationMessageFor()` method will only display an error if you have configured DataAnnotations attribute to the specifed property in the model class

## <a id="Routing">Routing</a>

### Attribute Based Routing

In Attribute Routing, Attributes are declared above the method so the route definitions are in close proximity to their correspondending actions. We can manage routes at controller level, action level and area level. Also, it can override the child route, if required. This provides a bigger level of flexibillity and thus Attribute Routing is the preferred option.

**Enable attribute routing**

You can enable attribute routing by calling `MapMvcAttributeRoutes` on the `RouteCollection` for your application (in `routeConfig`)

``` csharp
public class RouteConfig
{
    public static void RegisterRoutes(RouteCollection routes)
    {
        routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
        routes.MapMvcAttributeRoutes(); //Enables Attribute Routing
    }
}
```

A simple example of Attribute Based Routing:
``` csharp
[Route("Home/Index")] //Route: /Home/Index
public ActionResult Index() 
{ 
     ...
}
```

* The `[Route]` attribute specfies a route to be added to the route table.

Example of parametric attribute routing:
``` csharp
[Route("Users/{id}/{name?}")] //Route: /Users/12/Tim-Maes or /Users/12
public ActionResult Details(int id, string name) 
{ 
    ... 
}
```
* We specify parameters in the route by using curly brackets
* The name in the curly bracket matches the name of the `ActionResult`parameters.
* Optional parameters can be specified with a question mark: `{name?}`
* A constraint can also be specified: `{int:id}`

### Convention Based Routing

By default, you are given a route that will probably match most or perhaps all of your route. You can define custom routes if you have specific routes you would like to handle.

Example of a convention-based route

``` csharp
app.UseMvc(config =>{
    config.MapRoute(
    name: "Default",
    template: "{controller}/{action}/{id?}",
    defaults: new{ controller="Home", action="Index"}
); });
```

* Conventional-based routing is mainly just for returning HTML views in smaller applications.

Say we have this specific route:
``` csharp
routes.MapRoute(
    name: "UserDefault",
    url: "{controller}/{user}/{action}/{id}",
    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
);
```
... and this controller + methods:
``` csharp
public class HomeController : Controller
{
    public ActionResult Index(string user) 
    {
         ...
    }
    public ActionResult Documents(string user, int id) 
    { 
        ... 
    }
}
```

* URL of `/Home/SweetyUser/Documents/17`, will redirect to the Home controller's Documents action with `Sweety-User` as string parameter and `17` as id;

* URL of `/Home/SweetyUser`, we redirect to the `Index` action (specified in defaults) with `SweetyUser` as string parameter.

* URL of `/Home` we will be redirected to the Index. If no matching value is found for a given parameter, default value will be used.

## <a id="Globalization">Globalization</a>

A lot of web applications have translations available for their users. For example, in Belgium it is common to provide Dutch/Englisch/French translations. 
Using resources to store translations also prevents hardcoded `strings` in your views.

In order to externalize these translations in the form of `Resources`, we create a separate `Class Library` that store these resources and use the appropriate translations depending on the users `culture`.

For each language we add a `LANGUAGE.resx` resource file inside the resources `class library`.

**English**

{{< figure src="./../assets/resxeng.png" alt="English resources" >}}

**Dutch** 

{{< figure src="./../assets/resxnl.png" alt="Nederlands resources" >}}

* Note that the resource name of correspondending translations must be the same.

**ResX Manager Visual Studio Extension**

With `ResXManager` you can manage all resources in one view. Updated example of our demo application.

Download 
[ResXManager](https://haacked.com/archive/2011/01/06/razor-syntax-quick-reference.aspx/).

{{< figure src="./../assets/Resxmanager.png" alt="ResXManager" >}}

Inside our application, we can replace all hardcoded string with these translations. Below is an example of calling resources in our `user` table.

``` Csharp
<h2>Users</h2>
<div>
    <table class="table">
        <tr>
            <th scope="col">@Resources.Global.Nickname</th>
            <th scope="col">@Resources.Global.Fullname</th>
            <th scope="col">@Resources.Global.Emailaddress</th>
            <th scope="col">@Resources.Global.Role</th>
            <th scope="col">@Resources.Global.Actions</th>
        </tr>
        @foreach (var user in Model)
        {
            @Html.Partial("_UserPartial", user)
        }
    </table>
</div>
 ```

**Data Annotations example**

``` csharp
public class CreateUserViewModel
{
    [DisplayName(Resources.Name)]
    [Required(ErrorMessage=Resources.NameRequiredMessage)] 
    [MaxLength(100)]
    public string Name { get; set; }
}
```
 * In above example, the `errorMessage` will show the correct message, depending on user's selected culture. 

 The `DisplayName` attribute in above example is what will be displayed on the `View` when calling that property name.

 ```
 @Html.LabelFor(model => model.Name)
@Html.ValidationMessageFor(model => model.Name)
 ```

* Above ` HTML helpers` will display the correct translation depending on browser language / culture settings.

* You can use translations for every string on your application. You can store full paragraphs if needed. Just add the reference to the resources library and you can use them in your view simply by calling `@Resources.ResourceName`

## <a id="Razor-Syntax">Razor Syntax</a>

The `Razor View Enginge` is the most common used view engine in `ASP.NET MVC`. `Razor` allows you to write mix of `HTML` and server side code using `C#` (or `Visual Basic`).  

**Main Razor Syntax Rules**

* Code blocks are enclosed in @{...}
* Inline expressions start with @
* Statements end with a semicolon
* Variables are declared using `var`
* Strings are enclosed using quotation marks
* Comments are enclosed by @* ... *@

**Example using C#**

``` C#
<!-- Single statement block -->
@{ var message = "Hello World, welcome to Sweet Mustard"; }

<!-- Inline expression or variable -->
<p>The value of message is: @message</p> 

<!-- Multi-statement block -->
@{
var greeting = "Welcome to Sweet Mustard!";
var weekDay = DateTime.Now.DayOfWeek;
var greetingMessage = greeting + " Here in Kortrijk it is: " + weekDay;
}
<p>@greetingMessage</p>

@* comments *@
``` 
**Useful links**

[A quick reference guide](https://haacked.com/archive/2011/01/06/razor-syntax-quick-reference.aspx/)  on Razor syntax

## <a id="Javascript-Jquery-In-ASP">JavaScript / jQuery in ASP</a>

### JavaScript and jQuery in separate files

It is mandatory **not** to use JS/jQuery inside views. We always use separate files. In most cases we use a `.js` file per view, bepending on how complex the project/code is. If many views in an area have the same initialization, it's best practice to go with the base view approach. These files are typically placed in the `Scripts` folder.

JavaScript files are named after the view they're used in. F.e. `CreateUser.cshtml` view would have a corresponding `CreateUser.js` JavaScript file.
