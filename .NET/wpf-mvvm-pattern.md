---
title: "WPF MVVM"
menu:
  foundation:
    title: "WPF Model-View-ViewModel pattern"
    parent: ".NET"
weight: 8
---

## Introduction

In this article we describe some rules and best practices for XAML and MVVM application development. This applies to all XAML applications: WPF, Windows Store, Windows Phone and SilverLight projects.

## Project file structure

All XAML (MVVM) applications should have a directory structure *similar* to the example below.

```
<repository-root>
├ Controls (Reusable UI controls without view models like ASP.NET parials)
├ Localization (Classes and resources for application localization)
| └ .resx / .resw resource files
├ Models (Model and domain classes/objects)
├ ViewModels (View model classes)
| ├ MainWindowModel.cs
| ├ MyViewModel.cs
| └ Dialogs
|   └ SelectItemDialogModel.cs
├ Views (Contains the views)
| ├ MainWindow.xaml
| ├ MyView.xaml
| └ Dialogs
|   └ SelectItemDialog.xaml
└ App.xaml
```

As shown above, a name of a view should end with its type:

* `...Window`: A non-modal window
* `...Dialog`: A (modal) dialog window
* `...Page`: A page view (Phone and Windows Store Apps)
* `...View`: A view which is use as a subview in another view, page, window or dialog

The name of a view model is composed of the correspondending view name and the suffix `Model`. View models are stored in the same location in the _ViewModels_ directory as their correspondending views in de _View_ directory.

## Views and View Models

All views should follow the MVVM (Model-View-ViewModel) pattern to separate the UI from the business logic. There is always a 1-1 relationship between the view and viewmodel. If a view becomes too large, it is recommended to split it up in multiple views.

## User Controls versus Templated Controls

When a UI element is needed, you have to decide wether to use a _User Control_ (_View_ with _ViewModel_) or a _Templated Control_ (UI control).

General rule to choose:

* Create a _User Control_ when implementing an application specific view with a viewmodel and few dependency properties. These views should be in the _Views_ directory.
* Create a _Templated Control_ when implementing a reusable UI control without viewmodel and/or lots of dependency properties which are directly bound to XAML attributes. These controls should be in the _Controls_ directory.

More information about the differences between the two control types can be found [here](https://worldwidecode.wordpress.com/2011/02/01/choosing-between-user-control-and-template-control/).

## Visual representation of the MVVM pattern

{{< figure src="./../assets/MVVMchart.png" alt="MVVM-Chart" >}}

In some ways the MVVM pattern is similar to the MVP or MVC pattern. Both are _Separated Presentation_ patterns, and both are designed to isolate the details of the user interface from the underlying business logic in order to enhance manageability and testability.

For best practices regarding XAML code, read our [WPF Coding Conventions](./../wpf-coding-conventions) article.
