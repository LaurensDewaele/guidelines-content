---
title: .NET
menu: foundation
weight: 6
---

**Introduction**

In this section of the Design System website you can find our coding guidelines, best practices and conventions for software development in .NET. 
Click on a link in the sidebar to navigate through our modules. This is still a **work in progress** and will be updated and expanded on a frequent basis.
