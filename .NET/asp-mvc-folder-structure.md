---
title: "ASP.NET MVC Folder Structure"
menu:
  foundation:
    title: "ASP.NET MVC Folder Structure"
    parent: ".NET"
weight: 3
---

## Introduction

In this document we will address the folder structure in a standard ASP.NET MVC application.

### MVC and Convention Standard

By default, every ASP.NET MVC project heavily depends on conventions. For instance: MVC uses a convention-based directory-naming structure when resolving View templates and this allows you to omit the location path when referencing views from a Controller class f.e.
MVC is designed around sensible convention-based defaults that can be overridden as needed.

The ASP.NET MVC template has the convention over configuration implemented with the help of the following directories:

```
<root>
├ Controllers
├ Models
└ Views
```

### Key Points

* Each controller name ends with the suffix `Controller`.
* The single view directory can be used for the entire application.
* By default, all directories (folder) are created with the name of the Controller.

## Folder Structure

When creating a new ASP.NET MVC application using the VS built-in templates, the MVC folder structure is scaffolded by default. 

The main folders we can find are as following:

```
<root>
├ App_Data
├ App_Start
├ Content
├ Controllers
├ Models
├ Views
└ Scripts
```

{{< figure src="./../assets/MVCFolderStructure.png" alt="MVC Folder Structure" width="300em">}}

### App_Data folder

This folder is used to store file based databases, such as SQL _server.mdf_ files or _.xml_ files. Other application data, like an image cache, could also be stored in this folder.

### App_Start folder

This folder contains core configuration files that are applicable for the entire MVC application.

### Content folder

This folder contains UI related files such as CSS files, images and static files required by the UI.

{{< figure src="./../assets/MVCContentFolder.png" alt="Content folder" width="300em" >}}

### Controllers folder

This folder contains the _Controller_ files and their methods. These are responsible for processing user requests and return output.

{{< figure src="./../assets/MVCControllerFolder.png" alt="Controller folder" width="300em">}}

### Models folder

This folder contains the class files for models, entities and their properties.

{{< figure src="./../assets/MVCModelFolder.png" alt="Model folder" width="300em" >}}

### Views folder

This folder contains the UI pages and shared layout pages.

{{< figure src="./../assets/MVCViewFolder.png" alt="View folder" width="300em">}}

### Scripts folder

This folder contains script files and libraries such as JavaScript, JQuery, AngularJS, TypeScript, etc...

{{< figure src="./../assets/MVCScriptFolder.png" alt="Script folder" width="300em">}}

### Final note

This folder structure is not mandatory. You can rename it and adjust as desired by the project and architecture. There are several architectural patterns and application folder structures that can be used depending on the application size and purpose. 
