---
title: "ASP.NET MVC Architecture: Repository Pattern"
menu:
  foundation:
    title: "ASP.NET MVC Repository Pattern"
    parent: ".NET"
weight: 6
---

## Introduction

The standard ASP.NET MVC templates, adding controllers with scaffolding options and bootstrap the UI, is indeed a quick and efficient way to create your web application in just a few minutes.
But, this might not always be the right choice. Keeping business, data and presentation logic in the same project will impact several factors in your solution. These factors can be scalability, usability and testability.

In this document we will describe a loosely coupled ASP.NET MVC solution where Data Acces Layer (DAL), Business Logic (Service Layer) and Presentation Layer (UI) are separated.

**Navigation**

* [Technologies and frameworks](#taf)
* [Representation and folder structure](#visual)
* [Data Access Layer](#DAL)
    * [RepositoryBase](#RepositoryBase)
    * [UnitOfWork pattern](#uow)
* [Service Layer](#Service)
* [Presentation Layer](#Presentation)
* [AutoFac](#AutoFac)
* [AutoMapper](#AutoMapper)


**Technologies and Frameworks** 

<a id="taf">To demonstrate this architecture we will use the following patterns and frameworks listed below:</a>

* Entity Framework (Code First configuration with FluentAPI)
* A (Generic) Repository Pattern
* UnitOfWork Pattern
* Inversion of Control / Dependency Injection using Autofac
* AutoMapper for object mapping

<a id="visual">Below a visual representation of the 3-tier architecture.</a>

{{< figure src="./../assets/3-tier-chart.png" alt="3-tier chart" >}}

In our demo application, you can see the 3-tier architecture in the solution explorer like this:

```markdown
ProjectName (solution folder)
├ DAL (DataLayer) (TIER 1)
| ├ Repositories (folder)
| ├ Models (folder)
| └ DataAcces (folder)
├ ServiceLayer (TIER 2)
| ├ Services/
| └ Utils/
├ PresentationLayer (TIER 3)
| ├ Views/
| ├ Scripts/
| └ Content/
└ ...
```

## <a id="DAL">Data Access Layer (DAL): Repositories and Domain Models</a>

The purpose of the data layer is direct access to the database. This is the only layer responsible for communication with the database. If another layer needs access to the database, it will be done through repositories defined in the data layer project (`Store.Data`).

This project is where we install Entity Framework and configure the Entity Type Configurations in a subfolder for our Domain objects. Inside this project we create also a Configuration folder that wil hold each of these configurations (that inherits the EntityTypeConfiguration class).

### <a id="RepositoryBase">RepositoryBase</a>

This project also contains the abstract definition of the Generic Repository that holds all default operations like `Add`, `Update`, `Delete`, `GetById`. 

The `RepositoryBase` class is an abstract class that has virtual implementations of the `IRepository` interface. This base class will be inherited from all specific repositories and hence will implement the `IRepository` interface.

```csharp
public abstract class RepositoryBase<T> : IRepository
    where T : class
{
    private StoreEntities _dataContext;
    private readonly IDbSet<T> _dbSet;

    protected IDbFactory DbFactory
    {
        get;
        private set;
    }

    protected StoreEntities DbContext
    {
        get { return _dataContext ?? (_dataContext = DbFactory.Init()); }
    }

    protected RepositoryBase(IDbFactory dbFactory)
    {
        DbFactory = _dbFactory;
        _dbSet = DbContext.Set<T>();
    }

    public virtual void Add(T entity)
    {
        _dbSet.Add(entity);
    }

    public virtual void Update(T entity)
    {
        _dbSet.Attach(entity);
        _dataContext.Entry(entity).State = EntityState.Modified;
    }

    public virtual void Delete(T entity)
    {
        _dbSet.Remove(entity);
    }

    public virtual T GetById(int id)
    {
        return _dbSet.Find(id);
    }
}
```
### <a id="uow">UnitOfWork pattern</a>

`Unit of Work` is referred to as a single transaction that involves multiple operations of CRUD actions. In other words, when a `repository` implementation adds of updates a `entity` it does not send a command to the database. The `Commit` method of the `UnitOfWork` sends a command to the database (through the `IUnitOfWork` injected interface) to commit all transactions. 


**The UnitOfWork** 

This class receives the `DbContext` instance. The same class will further generate the requried repository instances and pass the `DbContext` in to both the repositories.

``` csharp
 public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory dbFactory;
        private StoreEntities dbContext;

        public UnitOfWork(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }

        public StoreEntities DbContext
        {
            get { return dbContext ?? (dbContext = dbFactory.Init()); }
        }

        public void Commit()
        {
            DbContext.Commit();
        }
    }

public interface IUnitOfWork
    {
        void Commit();
    }
```


### Repository per domain object

In the `Repositories` folder, we define a repository for each domain object. These repositories will further implement the abstract `RepositoryBase` class, in order to be able to execute default operations defined in `RepositoryBase`.

**Note:** A repository is like a collection of domain objects, so it should not return ViewModels/DTO's or anything that is not a domain object.

In our demo application, the `Data` folder also hold the domain models project (`Store.Model`).

{{< figure src="./../assets/repository-pattern-data-layer.png" alt="Data layer" width="350em" >}}

## <a id="Service">Service Layer</a>

Do we want to expose operations in MVC controllers? Where do we need to implement business logic? 
We keep business logic in the Service layer. This is also a seperate `Class Library Project`.
To access the database, we will not install Entity Framework in this project. Any database operation will be done through injected repository interfaces.

Below the `GadgetService` class (snippet) from our demo application, where the repository interfaces and `UnitOfWork` interface are injected in the constructor.

```csharp
public class GadgetService : IGadgetService
{
    // Private fields to store injected dependencies
    private readonly IGadgetRepository _gadgetsRepository;
    private readonly ICategoryRepository _categoryRepository;
    private readonly IUnitOfWork _unitOfWork;

    // Instances of the repositories are injected in the constructor
    public GadgetService(IGadgetRepository gadgetsRepository, ICategoryRepository categoryRepository, IUnitOfWork unitOfWork)
    {
        _gadgetsRepository = gadgetsRepository;
        _categoryRepository = categoryRepository;
        _unitOfWork = unitOfWork;
    }

    public IEnumerable<Gadget> GetGadgets()
    {
        var gadgets = _gadgetsRepository.GetAll();
        return gadgets;
    }

    public IEnumerable<Gadget> GetCategoryGadgets(string categoryName, string gadgetName = null)
    {
        var category = _categoryRepository.GetCategoryByName(categoryName);
        return _category.Gadgets.Where(g => g.Name.Contains(gadgetName));
    }

    public Gadget GetGadget(int id)
    {
        var gadget = _gadgetsRepository.GetById(id);
        return gadget;
    }

    // ...
}
``` 

## <a id="Presentation">Presentation Layer</a>

This layer is the `Web.UI` layer, the actual ASP.MVC project.
References must be made to previous layers, and the Entity Framework package needs to be installed in this project to get access to some namespaces in order to set up the database configurations, such as the Database Initializer.

### Controllers and Views

The main purpose of the controllers is sending/receiving ViewModels, from/to the Views and Service layer.

Each controller has private fields to store Service dependencies. These are injected through Constructor Injection with each request and stored in those private fields for the lifetime of a single request.

#### HomeController example

```csharp
public class HomeController : Controller
{
    #region Private Fields
    
    private readonly ICategoryService _categoryService;
    private readonly IGadgetService _gadgetService;

    #endregion

    #region Constructor

    public HomeController(ICategoryService categoryService, IGadgetService gadgetService)
    {
        _categoryService = categoryService;
        _gadgetService = gadgetService;
    }

    #endregion

    #region Views

    public ActionResult Index(string category = null)
    {
        var categories = _categoryService.GetCategories(category).ToList();
        var viewModelGadgets = Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryViewModel>>(categories);
        return View(viewModelGadgets);
    }

    #endregion

    // ...
}
```

### <a id="AutoFac">Dependency Injection using AutoFac</a>

We will use Autofac to get dependencies injected into our MVC controllers and to manage the lifetime of these dependencies.

Autofac and the Autofac MVC integration package will be installed in the `Web.UI` project. In the `Bootstrapper.cs` file under the `App_Start` folder, we register our services, repositories, `UnitOfWork` and controllers through Autofac's `ContainerBuilder`. During application execution and for each request, Autofac will create a lifetime scope from which dependencies will get resolved.

```csharp
public class Bootstrapper
{
    public static void Run()µ
    {
        SetAutofacContainer();
        AutoMapperConfiguration.Configure();
    }

    private static void SetAutofacContainer()
    {
        var builder = new ContainerBuilder();

        builder.RegisterControllers(Assembly.GetExecutingAssembly());

        builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerLifetimeScope();

        builder.RegisterAssemblyTypes(typeof(GadgetRepository).Assembly)
            .Where(t => t.Name.EndsWith("Repository"))
            .AsImplementedInterfaces().InstancePerLifetimeScope();

        builder.RegisterAssemblyTypes(typeof(GadgetService).Assembly)
           .Where(t => t.Name.EndsWith("Service"))
           .AsImplementedInterfaces().InstancePerLifetimeScope();

        var container = builder.Build();
        DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
    }
}
```

The idea behind inversion of control is that, rather than tie the classes in your application together and let classes “new up” their dependencies, you switch it around so dependencies are instead passed in during class construction. This makes it easier to write unit-tests by passing in mocked representations of those dependencies.

### <a id="AutoMapper">Object Mapping with AutoMapper</a>

When a view only needs a few properties of your objects, or when creating/posting object from a form, you only want to post specific domain objects properties.

For this reason we define ViewModel objects and use them instead of the domain objects. 
This is where AutoMapper is being used, to map the ViewModels properties to the Domain Objects.

**Mapping Profiles**

For Domain -> ViewModel mappings we don't need to set up that much. AutoMapper uses the default conventions.

``` csharp
public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }
 
        protected override void Configure()
        {
            Mapper.CreateMap<Category,CategoryViewModel>();
            Mapper.CreateMap<Gadget, GadgetViewModel>();
        }
    }
```

For ViewModel => Domain mapping we set the configuration manually as shown in the code snippet below, ÀutoMapper` can't use the default conventions because property names don't match.

``` csharp
public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }
 
        protected override void Configure()
        {
            Mapper.CreateMap<GadgetFormViewModel, Gadget>()
                .ForMember(g => g.Name, map => map.MapFrom(vm => vm.GadgetTitle))
                .ForMember(g => g.Description, map => map.MapFrom(vm => vm.GadgetDescription))
                .ForMember(g => g.Price, map => map.MapFrom(vm => vm.GadgetPrice))
                .ForMember(g => g.Image, map => map.MapFrom(vm => vm.File.FileName))
                .ForMember(g => g.CategoryID, map => map.MapFrom(vm => vm.GadgetCategory));
        }
    }
```
>If Domain Object and ViewModel have properties with the same name and type, AutoMapper is smart enough to use the mapping through default conventions.

Initialize the mappings in the `Mapper.Initialize` method in the `AutoMapperConfiguration`file.

``` csharp
public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.AddProfile<ViewModelToDomainMappingProfile>();
            });
        }
    }
```