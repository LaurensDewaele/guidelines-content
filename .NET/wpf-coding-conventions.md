---
title: "WPF Coding Conventions"
menu:
  foundation:
    title: "WPF Coding conventions"
    parent: ".NET"
weight: 7
---

## XAML Conventions

XAML is an XML based markup language so the majority of the XML standards apply to XAML.

### Use X:Name instead of Name

While they both do the same thing, `Name` can only be used for some elements, where `x:Name` will work for any element.
Also, when looking through a long list of attributes on an element, the attributes starting with `x:` are easier to spot.

### Place the first attribute of an element on the line underneath the element name

```xml
<!--Don't-->
<StackPanel DockPanel.Dock="Top" Orientation="Horizontal"
            Margin="3">
    <Button>
```

Change them so that the first attribute is on a new line, and put the closing angle bracket on its own line as well, like this:

```xml
<StackPanel
    DockPanel.Dock="Top"
    Orientation="Horizontal"
    Margin="3"
    >
    <Button>
```

This places all attributes on the same indentation level.

**Exception: the elements `x:Name` attribute**

This can be put next to element name for convenience purposes:

```xml
<StackPanel x:Name="PanelName"
    DockPanel.Dock="Top"
    Orientation="Horizontal"
    Margin="3"
    >
    <Button>
```

### If an element has no content, self-close it

Using a closing tag on an empty element is a waste of space. Always use a self-closing tag in this situation.

```xml
<!-- Do this -->
<Button x:Name="SomeButton"
    Text="Hello Sweet Mustard" 
    />

<!-- Avoid -->
<Button x:Name="SomeButton"
    Text="Hello Sweet Mustard" 
    ></Button>
```

### Suffix XAML with a type indication

The name of an element should always have a hint about the elements type at the end of the name.

```xml
<!-- Do this -->
<Button x:Name="CheckoutButton"
    Text="Checkout" 
    />

<!-- Avoid -->
<Button x:Name="Checkout"
    Text="Checkout" 
    />
``` 

### Only name nodes that are used in code behind or element binding

```xml
<!-- If name/key is referenced in code-behind: -->
<StackPanel x:Name="ActionsPanel">
    <!-- ... -->
</StackPanel>

<!-- If there's no reference to the node in code-behind: -->
<StackPanel>
    <!-- ... -->
</StackPanel>
```
